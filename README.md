
Under Development

Movies Database Application 


<img src ="/screens/1.png" width="270" height="480">
<img src ="/screens/2.png" width="270" height="480">
<img src ="/screens/3.png" width="270" height="480">
<img src ="/screens/4.png" width="270" height="480">
<img src ="/screens/5.png" width="270" height="480">
<img src ="/screens/6.png" width="270" height="480">


<b><tr>Implementations </tr></b>

<p><tr>MVP Architecture Design Pattern </tr></p>
<p><tr>Rxjava and RxAndroid </tr></p>
<p><tr>RxBus </tr></p>
<p><tr>Dagger2 - Dependency Injection </tr></p>
<p><tr>ButterKnife -Dependency Injection UI </tr></p>
<p><tr>Retrofit2 - Network Service </tr></p>
<p><tr>Fresco - Image Loading </tr></p>
<p><tr>Guava - Manupulating Collections </tr></p>
<p><tr>LanbdaExpression </tr></p>
<p><tr>Material Design </tr></p>
<p><tr>Room Database - Local Storage </tr></p>
<p><tr>Lombok Library - Optimizing Objects </tr></p>
<p><tr>Espressco - unit Testing </tr></p>
<p><tr>Facebook , Twitter, Google plus Integrations </tr></p>
<p><tr>CrashLytics  </tr></p>
<p><tr> </tr></p>


dependencies {
    implementation fileTree(include: ['*.jar'], dir: 'libs')
    implementation "com.android.support:appcompat-v7:$rootProject.supportLibraryVersion"
    implementation 'com.android.support.constraint:constraint-layout:1.0.2'
    testImplementation 'junit:junit:4.12'
    androidTestImplementation 'com.android.support.test:runner:1.0.1'
    androidTestImplementation 'com.android.support.test.espresso:espresso-core:3.0.1'
    implementation "com.android.support:design:$rootProject.supportLibraryVersion"
    implementation "com.android.support:cardview-v7:$rootProject.supportLibraryVersion"
    implementation "com.android.support:recyclerview-v7:$rootProject.supportLibraryVersion"

    //Dagger
    implementation "com.google.dagger:dagger:$rootProject.dagger2Version"
    annotationProcessor "com.google.dagger:dagger-compiler:$rootProject.dagger2Version"

    //Retrofit
    implementation "com.squareup.retrofit2:retrofit:$rootProject.retrofitVersion"
    implementation "com.squareup.retrofit2:converter-gson:$rootProject.retrofitVersion"

    //OkHttp
    implementation "com.squareup.okhttp3:logging-interceptor:$rootProject.okhttpVersion"
    implementation "com.squareup.okhttp3:okhttp:$rootProject.okhttpVersion"

    //Gson
    implementation "com.google.code.gson:gson:$rootProject.gsonVersion"

    //Fresco
    implementation "com.facebook.fresco:fresco:$rootProject.frescoVersion"

    //RxJava
    implementation "io.reactivex.rxjava2:rxjava:$rootProject.rxjava2Version"
    implementation "io.reactivex.rxjava2:rxandroid:$rootProject.rxandroidVersion"
    implementation 'com.jakewharton.retrofit:retrofit2-rxjava2-adapter:1.0.0'
    implementation 'com.android.support:multidex:1.0.2'

    //Lombok
    compileOnly "org.projectlombok:lombok:$rootProject.lombokVersion"
    annotationProcessor "org.projectlombok:lombok:$rootProject.lombokVersion"

    //butter knife
    implementation "com.jakewharton:butterknife:$rootProject.butterKnifeVersion"
    annotationProcessor "com.jakewharton:butterknife-compiler:$rootProject.butterKnifeVersion"

    //Timber
    implementation 'com.jakewharton.timber:timber:4.5.1'
    provided 'javax.annotation:jsr250-api:1.0'
    implementation 'javax.inject:javax.inject:1'

    //Guava
    implementation 'com.google.guava:guava:24.0-android'

    //androidTestCompile 'com.google.code.findbugs:jsr305:3.0.1'
    implementation 'android.arch.persistence.room:runtime:' + rootProject.archRoomVersion
    implementation 'android.arch.persistence.room:rxjava2:' + rootProject.archRoomVersion
    annotationProcessor 'android.arch.persistence.room:compiler:' + rootProject.archRoomVersion

    //  implementation 'com.facebook.android:facebook-android-sdk:[4,5)'
    implementation 'com.github.orangegangsters:swipy:1.2.3@aar'

    //  compile 'com.github.florent37:rxbus:1.0.0'
}













