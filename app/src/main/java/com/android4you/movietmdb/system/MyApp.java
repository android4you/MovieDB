package com.android4you.movietmdb.system;

import android.app.Application;

import com.android4you.movietmdb.data.DataManager;
import com.android4you.movietmdb.di.component.ApplicationComponent;
import com.android4you.movietmdb.di.component.DaggerApplicationComponent;
import com.android4you.movietmdb.di.module.ApplicationModule;
import com.android4you.movietmdb.system.bus.RxBus;
import com.facebook.drawee.backends.pipeline.Fresco;

import javax.inject.Inject;

/**
 * Created by manu on 2/18/2018.
 */

public class MyApp extends Application {

    @Inject
    DataManager mDataManager;

    private RxBus bus;

    private ApplicationComponent mApplicationComponent;
    private boolean isListorGrid = true;

    @Override
    public void onCreate() {
        super.onCreate();

        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this)).build();
        mApplicationComponent.inject(this);
        Fresco.initialize(this);
       // AppLogger.init();
        bus = new RxBus();

    }

    public ApplicationComponent getComponent() {
        return mApplicationComponent;
    }


    // Needed to replace the component with a test specific one
    public void setComponent(ApplicationComponent applicationComponent) {
        mApplicationComponent = applicationComponent;
    }
    public RxBus bus() {
        return bus;
    }

    public void setListGrid(boolean b){
        isListorGrid = b;
    }
    public boolean getList(){
        return isListorGrid;
    }

}
