package com.android4you.movietmdb.data.model;

/**
 * Created by manu on 4/30/2018.
 */

public class TvShowsCombinationModel {

    public TvShowsCombinationModel(TVShowsModel popular, TVShowsModel topRated, TVShowsModel onAir, TVShowsModel airingToday) {
        this.popular = popular;
        this.airingToday = airingToday;
        this.onAir = onAir;
        this.topRated = topRated;
    }

    public TVShowsModel popular;
    public TVShowsModel airingToday;
    public TVShowsModel onAir;
    public TVShowsModel topRated;
}