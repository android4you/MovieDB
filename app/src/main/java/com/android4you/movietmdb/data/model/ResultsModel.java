package com.android4you.movietmdb.data.model;

import lombok.Data;

/**
 * Created by Manu on 4/27/2018.
 */
@Data
public class ResultsModel {

    private int vote_count;
    private int id;
    private boolean video;
   // private int vote_average;
    private String title;
  //  private double popularity;
    private String poster_path;
    private String original_language;
    private String original_title;
    private String backdrop_path;
 //   private boolean adult;
    private String overview;
    private String release_date;
}
