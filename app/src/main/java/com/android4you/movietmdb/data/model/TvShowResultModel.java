package com.android4you.movietmdb.data.model;

import java.util.List;

import lombok.Data;

/**
 * Created by manu on 4/29/2018.
 */
@Data
public class TvShowResultModel {

    private String original_name;
    private String name;
    private double popularity;
    private int vote_count;
    private String first_air_date;
    private String backdrop_path;
    private String original_language;
    private int id;
    private double vote_average;
    private String overview;
    private String poster_path;
    private List<Integer> genre_ids;
    private List<String> origin_country;
}
