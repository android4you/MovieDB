package com.android4you.movietmdb.data.network;


import com.android4you.movietmdb.data.model.MovieModel;
import com.android4you.movietmdb.data.model.PopularPeopleModel;
import com.android4you.movietmdb.data.model.TVShowsModel;
import com.android4you.movietmdb.data.model.people.MovieCreditsModel;
import com.android4you.movietmdb.data.model.people.PeopleDetailModel;
import com.android4you.movietmdb.data.model.people.TvShowsCreditsModel;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by manu on 2/18/2018.
 */

public interface ApiService {

    @GET("movie/popular?")
    Observable<MovieModel> getPopularMovies(@Query("api_key") String api_key, @Query("page") String page);

    @GET("movie/top_rated?")
    Observable<MovieModel> getTopRatedMovies(@Query("api_key") String api_key, @Query("page") String page);

    @GET("movie/now_playing?")
    Observable<MovieModel> getNowPlayingMovies(@Query("api_key") String api_key, @Query("page") String page);

    @GET("movie/upcoming?")
    Observable<MovieModel> getUpcomingMovies(@Query("api_key") String api_key, @Query("page") String page);

    @GET("tv/popular?")
    Observable<TVShowsModel> getPopularTVs(@Query("api_key") String api_key, @Query("page") String page);

    @GET("tv/top_rated?")
    Observable<TVShowsModel> getTopRatedTVs(@Query("api_key") String api_key, @Query("page") String page);

    @GET("tv/on_the_air?")
    Observable<TVShowsModel> getOnAirTVs(@Query("api_key") String api_key, @Query("page") String page);

    @GET("tv/airing_today?")
    Observable<TVShowsModel> getAiringToday(@Query("api_key") String api_key, @Query("page") String page);

    @GET("person/popular?")
    Observable<PopularPeopleModel> getpopularPeople(@Query("api_key") String api_key, @Query("page") String page);

    @GET("discover/movie?sort_by=popularity.desc")
    Observable<MovieModel> getDiscoverMovies(@Query("api_key") String api_key, @Query("page") String page);

    @GET("person/{person_id}?")
    Observable<PeopleDetailModel> getPerson(@Path("person_id") String person_id, @Query("api_key") String api_key);

    @GET("person/{person_id}/movie_credits?")
    Observable<MovieCreditsModel> getPersonMovies(@Path("person_id") String person_id, @Query("api_key") String api_key);

    @GET("person/{person_id}/tv_credits")
    Observable<TvShowsCreditsModel> getPersonTVShows(@Path("person_id") String person_id, @Query("api_key") String api_key);

}
