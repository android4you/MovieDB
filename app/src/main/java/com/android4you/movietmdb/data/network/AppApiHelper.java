package com.android4you.movietmdb.data.network;


import com.android4you.movietmdb.data.model.MovieModel;
import com.android4you.movietmdb.data.model.PopularPeopleModel;
import com.android4you.movietmdb.data.model.TVShowsModel;
import com.android4you.movietmdb.data.model.people.MovieCreditsModel;
import com.android4you.movietmdb.data.model.people.PeopleDetailModel;
import com.android4you.movietmdb.data.model.people.TvShowsCreditsModel;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import retrofit2.Retrofit;

/**
 * Created by manu on 2/18/2018.
 */
@Singleton
public class AppApiHelper implements IApiHelper {


    private ApiService mApiService;

    @Inject
    public AppApiHelper(Retrofit ref) {
        mApiService = ref.create(ApiService.class);
    }



    @Override
    public Observable<MovieModel> getPopularMovies(String api_key, String page) {
        return mApiService.getPopularMovies(api_key,page);
    }

    @Override
    public Observable<MovieModel> getUpcomingMovies(String api_key, String page) {
        return mApiService.getUpcomingMovies(api_key,page);
    }

    @Override
    public Observable<MovieModel> getTopRatedMovies(String api_key, String page) {
        return mApiService.getTopRatedMovies(api_key,page);
    }

    @Override
    public Observable<MovieModel> getNowPlayingMovies(String api_key, String page) {
        return mApiService.getNowPlayingMovies(api_key,page);
    }

    @Override
    public Observable<TVShowsModel> getPopularTVs(String api_key, String page) {
        return mApiService.getPopularTVs(api_key,page);  }

    @Override
    public Observable<TVShowsModel> getTopRatedTVs(String api_key, String page) {
        return mApiService.getTopRatedTVs(api_key,page);
    }

    @Override
    public Observable<TVShowsModel> getOnAirTVs(String api_key, String page) {
        return mApiService.getOnAirTVs(api_key,page);
    }

    @Override
    public Observable<TVShowsModel> getAiringToday(String api_key, String page) {
        return mApiService.getAiringToday(api_key,page);
    }

    @Override
    public Observable<PopularPeopleModel> getpopularPeople(String api_key, String page) {
        return mApiService.getpopularPeople(api_key,  page);
    }

    @Override
    public Observable<MovieModel> getDiscoverMovies(String api_key, String page) {
        return mApiService.getDiscoverMovies(api_key, page);
    }

    @Override
    public Observable<PeopleDetailModel> getPerson(String person_id, String api_key) {
        return mApiService.getPerson(person_id,api_key);
    }

    @Override
    public Observable<MovieCreditsModel> getPersonMovies(String person_id, String api_key) {
        return mApiService.getPersonMovies(person_id,api_key);
    }

    @Override
    public Observable<TvShowsCreditsModel> getPersonTVShows(String person_id, String api_key) {
        return mApiService.getPersonTVShows(person_id, api_key);
    }


}