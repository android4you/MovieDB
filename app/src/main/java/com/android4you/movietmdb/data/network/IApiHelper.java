package com.android4you.movietmdb.data.network;


import com.android4you.movietmdb.data.model.MovieModel;
import com.android4you.movietmdb.data.model.PopularPeopleModel;
import com.android4you.movietmdb.data.model.TVShowsModel;
import com.android4you.movietmdb.data.model.people.MovieCreditsModel;
import com.android4you.movietmdb.data.model.people.PeopleDetailModel;
import com.android4you.movietmdb.data.model.people.TvShowsCreditsModel;

import io.reactivex.Observable;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by manu on 2/18/2018.
 */

public interface IApiHelper {

    Observable<MovieModel> getPopularMovies(String api_key, String page);

    Observable<MovieModel> getUpcomingMovies(String api_key, String page);

    Observable<MovieModel> getTopRatedMovies(String api_key, String page);

    Observable<MovieModel> getNowPlayingMovies(String api_key, String page);


    Observable<TVShowsModel> getPopularTVs(String api_key, String page);

    Observable<TVShowsModel> getTopRatedTVs(String api_key, String page);

    Observable<TVShowsModel> getOnAirTVs(String api_key, String page);

    Observable<TVShowsModel> getAiringToday(String api_key, String page);


    Observable<PopularPeopleModel> getpopularPeople(String api_key, String page);

    Observable<MovieModel> getDiscoverMovies(String api_key, String page);

    Observable<PeopleDetailModel> getPerson(String api_key, String person_id);

    Observable<MovieCreditsModel> getPersonMovies(String person_id, String api_key);

    Observable<TvShowsCreditsModel> getPersonTVShows(String person_id, String api_key);

}
