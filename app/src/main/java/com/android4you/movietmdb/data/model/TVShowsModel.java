package com.android4you.movietmdb.data.model;

import java.util.List;

import lombok.Data;

/**
 * Created by manu on 4/29/2018.
 */
@Data
public class TVShowsModel {


    private int page;
    private int total_results;
    private int total_pages;
    private List<TvShowResultModel> results;


}
