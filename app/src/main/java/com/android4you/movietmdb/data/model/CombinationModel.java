package com.android4you.movietmdb.data.model;

/**
 * Created by Manu on 4/27/2018.
 */

public class CombinationModel {

    public CombinationModel(MovieModel popular, MovieModel upcoming, MovieModel nowplaying, MovieModel topRated) {
        this.popular = popular;
        this.upcoming = upcoming;
        this.nowplaying = nowplaying;
        this.topRated = topRated;
    }

    public MovieModel popular;
    public MovieModel upcoming;
    public MovieModel nowplaying;
    public MovieModel topRated;
}
