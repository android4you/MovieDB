package com.android4you.movietmdb.data.model;

import java.util.List;

import lombok.Data;


/**
 * Created by Manu on 4/27/2018.
 */
@Data
public class MovieModel {

    private int page;
    private int total_results;
    private int total_pages;
    private List<ResultsModel> results;

}
