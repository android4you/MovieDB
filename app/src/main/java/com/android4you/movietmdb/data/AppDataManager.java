package com.android4you.movietmdb.data;

import android.content.Context;

import com.android4you.movietmdb.data.model.MovieModel;
import com.android4you.movietmdb.data.model.PopularPeopleModel;
import com.android4you.movietmdb.data.model.TVShowsModel;
import com.android4you.movietmdb.data.model.people.MovieCreditsModel;
import com.android4you.movietmdb.data.model.people.PeopleDetailModel;
import com.android4you.movietmdb.data.model.people.TvShowsCreditsModel;
import com.android4you.movietmdb.data.network.IApiHelper;
import com.android4you.movietmdb.di.scope.ApplicationContext;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

/**
 * Created by manu on 2/18/2018.
 */
@Singleton
public class AppDataManager implements DataManager {

    /**
     * Application context
     */
    private final Context mContext;

    /**
     * API Helper for network calls
     */
    private final IApiHelper mApiHelper;


    @Inject
    public AppDataManager(@ApplicationContext Context context, IApiHelper apiHelper) {
        mContext = context;
        mApiHelper = apiHelper;
    }

    @Override
    public Observable<MovieModel> getPopularMovies(String api_key, String page) {
        return mApiHelper.getPopularMovies(api_key,page).map(productModel -> productModel);
    }

    @Override
    public Observable<MovieModel> getUpcomingMovies(String api_key, String page) {
        return mApiHelper.getUpcomingMovies(api_key,page).map(productModel -> productModel);
    }

    @Override
    public Observable<MovieModel> getTopRatedMovies(String api_key, String page) {
        return mApiHelper.getTopRatedMovies(api_key,page).map(productModel -> productModel);
    }

    @Override
    public Observable<MovieModel> getNowPlayingMovies(String api_key, String page) {
        return mApiHelper.getNowPlayingMovies(api_key,page).map(productModel -> productModel);
    }

    @Override
    public Observable<TVShowsModel> getPopularTVs(String api_key, String page) {
        return  mApiHelper.getPopularTVs(api_key,page).map(productModel -> productModel);
    }

    @Override
    public Observable<TVShowsModel> getTopRatedTVs(String api_key, String page) {
        return  mApiHelper.getTopRatedTVs(api_key,page).map(productModel -> productModel);
    }

    @Override
    public Observable<TVShowsModel> getOnAirTVs(String api_key, String page) {
        return mApiHelper.getOnAirTVs(api_key,page).map(productModel -> productModel);
    }

    @Override
    public Observable<TVShowsModel> getAiringToday(String api_key, String page) {
        return mApiHelper.getAiringToday(api_key,page).map(productModel -> productModel);
    }

    @Override
    public Observable<PopularPeopleModel> getpopularPeople(String api_key,String page) {
        return mApiHelper.getpopularPeople(api_key,page).map(productModel -> productModel);
    }


    @Override
    public Observable<MovieModel> getDiscoverMovies(String api_key, String page) {
        return mApiHelper.getDiscoverMovies(api_key,page).map(productModel -> productModel);
    }

    @Override
    public Observable<PeopleDetailModel> getPerson(String person_id, String api_key) {
        return mApiHelper.getPerson(person_id, api_key);
    }

    @Override
    public Observable<MovieCreditsModel> getPersonMovies(String person_id, String api_key) {
        return mApiHelper.getPersonMovies(person_id,api_key);
    }

    @Override
    public Observable<TvShowsCreditsModel> getPersonTVShows(String person_id, String api_key) {
        return mApiHelper.getPersonTVShows(person_id, api_key);
    }
}

