package com.android4you.movietmdb.data.model;

import java.util.List;

/**
 * Created by manu on 4/29/2018.
 */

public class PopularResponseModel {
    private int page;
    private int total_results;
    private int total_pages;
    private List<ResultsModel> results;
}
