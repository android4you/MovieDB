package com.android4you.movietmdb.data.model;

import com.android4you.movietmdb.data.model.people.MovieCreditsModel;
import com.android4you.movietmdb.data.model.people.PeopleDetailModel;
import com.android4you.movietmdb.data.model.people.TvShowsCreditsModel;

/**
 * Created by manu on 7/10/2018.
 */

public class DetailCombinedModel {

    public DetailCombinedModel(PeopleDetailModel detailModel, MovieCreditsModel movieCreditsModel, TvShowsCreditsModel tvShowsCreditsModel) {

        this.detailModel = detailModel;
        this.movieCreditsModel = movieCreditsModel;
        this.tvShowsCreditsModel = tvShowsCreditsModel;

    }

    public PeopleDetailModel detailModel;
    public MovieCreditsModel movieCreditsModel;
    public TvShowsCreditsModel tvShowsCreditsModel;

}
