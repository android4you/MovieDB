package com.android4you.movietmdb.data.model.genre;

import lombok.Data;

/**
 * Created by manu on 5/30/2018.
 */
@Data
public class GenresResultModel {

    private int id;
    private String name;
}
