package com.android4you.movietmdb.data.model.genre;


import com.android4you.movietmdb.data.model.genre.GenresResultModel;

import java.util.List;

import lombok.Data;

/**
 * Created by manu on 5/30/2018.
 */
@Data
public class GenreModel {
    private List<GenresResultModel> genres;

}
