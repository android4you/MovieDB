package com.android4you.movietmdb.di.module;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;

import com.android4you.movietmdb.di.scope.ActivityContext;
import com.android4you.movietmdb.di.scope.PerActivity;
import com.android4you.movietmdb.ui.base.BaseActivity;
import com.android4you.movietmdb.ui.discover.DiscoverAdapter;
import com.android4you.movietmdb.ui.discover.DiscoverPresenter;
import com.android4you.movietmdb.ui.discover.DiscoverPresenterImpl;
import com.android4you.movietmdb.ui.discover.DiscoverView;
import com.android4you.movietmdb.ui.genres.GenresPresenter;
import com.android4you.movietmdb.ui.genres.GenresPresenterImpl;
import com.android4you.movietmdb.ui.genres.GenresView;
import com.android4you.movietmdb.ui.home.HomePresenter;
import com.android4you.movietmdb.ui.home.HomePresenterImpl;
import com.android4you.movietmdb.ui.home.HomeView;
import com.android4you.movietmdb.ui.landing.LandingPresenter;
import com.android4you.movietmdb.ui.landing.LandingPresenterImpl;
import com.android4you.movietmdb.ui.landing.LandingView;
import com.android4you.movietmdb.ui.landing.MoviesTVShowsAdapter;
import com.android4you.movietmdb.ui.landing.PoepleAdapter;
import com.android4you.movietmdb.ui.landing.TVShowsAdapter;
import com.android4you.movietmdb.ui.movies.nowplayiing.NowPlayingAdapter;
import com.android4you.movietmdb.ui.movies.nowplayiing.NowPlayingPresenter;
import com.android4you.movietmdb.ui.movies.nowplayiing.NowPlayingPresenterImpl;
import com.android4you.movietmdb.ui.movies.nowplayiing.NowPlayingView;
import com.android4you.movietmdb.ui.movies.popular.PopularAdapter;
import com.android4you.movietmdb.ui.movies.popular.PopularPresenter;
import com.android4you.movietmdb.ui.movies.popular.PopularPresenterImpl;
import com.android4you.movietmdb.ui.movies.popular.PopularView;
import com.android4you.movietmdb.ui.people.PopularPeopleAdapter;
import com.android4you.movietmdb.ui.people.PopularPeoplePresenter;
import com.android4you.movietmdb.ui.people.PopularPeoplePresenterImpl;
import com.android4you.movietmdb.ui.people.PopularPeopleView;
import com.android4you.movietmdb.ui.people.details.PeopleDetailsPresenter;
import com.android4you.movietmdb.ui.people.details.PeopleDetailsPresenterImpl;
import com.android4you.movietmdb.ui.people.details.PeopleDetailsView;
import com.android4you.movietmdb.ui.people.details.crew.PeopleCrewAdapter;
import com.android4you.movietmdb.ui.people.details.crew.PeopleCrewPresenter;
import com.android4you.movietmdb.ui.people.details.crew.PeopleCrewPresenterImpl;
import com.android4you.movietmdb.ui.people.details.crew.PeopleCrewView;
import com.android4you.movietmdb.ui.people.details.movies.PeopleMovieCreditAdapter;
import com.android4you.movietmdb.ui.people.details.movies.PeopleMovieCreditPresenter;
import com.android4you.movietmdb.ui.people.details.movies.PeopleMovieCreditPresenterImpl;
import com.android4you.movietmdb.ui.people.details.movies.PeopleMovieCreditView;
import com.android4you.movietmdb.ui.people.details.tvshows.PeopleTVShowCreditView;
import com.android4you.movietmdb.ui.people.details.tvshows.PeopleTVShowPresenter;
import com.android4you.movietmdb.ui.people.details.tvshows.PeopleTVShowPresenterImpl;
import com.android4you.movietmdb.ui.people.details.tvshows.PeopleTVshowsCreditAdapter;
import com.android4you.movietmdb.ui.splash.SplashPresenter;
import com.android4you.movietmdb.ui.splash.SplashPresenterImpl;
import com.android4you.movietmdb.ui.splash.SplashView;
import com.android4you.movietmdb.ui.movies.toprated.TopRatedAdapter;
import com.android4you.movietmdb.ui.movies.toprated.TopRatedPresenter;
import com.android4you.movietmdb.ui.movies.toprated.TopRatedPresenterImpl;
import com.android4you.movietmdb.ui.movies.toprated.TopRatedView;
import com.android4you.movietmdb.ui.movies.upcoming.UpComingAdapter;
import com.android4you.movietmdb.ui.movies.upcoming.UpComingPresenter;
import com.android4you.movietmdb.ui.movies.upcoming.UpComingPresenterImpl;
import com.android4you.movietmdb.ui.movies.upcoming.UpComingView;
import com.android4you.movietmdb.ui.tvshows.AiringToday.AiringTodayTvAdapter;
import com.android4you.movietmdb.ui.tvshows.AiringToday.AiringTodayTvPresenter;
import com.android4you.movietmdb.ui.tvshows.AiringToday.AiringTodayTvPresenterImpl;
import com.android4you.movietmdb.ui.tvshows.AiringToday.AiringTodayTvView;
import com.android4you.movietmdb.ui.tvshows.onAir.OnAirTvAdapter;
import com.android4you.movietmdb.ui.tvshows.onAir.OnAirTvPresenter;
import com.android4you.movietmdb.ui.tvshows.onAir.OnAirTvPresenterImpl;
import com.android4you.movietmdb.ui.tvshows.onAir.OnAirTvView;
import com.android4you.movietmdb.ui.tvshows.popular.PopularTvAdapter;
import com.android4you.movietmdb.ui.tvshows.popular.PopularTvPresenter;
import com.android4you.movietmdb.ui.tvshows.popular.PopularTvPresenterImpl;
import com.android4you.movietmdb.ui.tvshows.popular.PopularTvView;
import com.android4you.movietmdb.ui.tvshows.toprated.TopRatedTvAdapter;
import com.android4you.movietmdb.ui.tvshows.toprated.TopRatedTvPresenter;
import com.android4you.movietmdb.ui.tvshows.toprated.TopRatedTvPresenterImpl;
import com.android4you.movietmdb.ui.tvshows.toprated.TopRatedTvView;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by manu on 2/18/2018.
 */

@Module
public class ActivityModule {

    private AppCompatActivity mActivity;

    public ActivityModule(AppCompatActivity activity) {
        this.mActivity = activity;
    }

    @Provides
    @ActivityContext
    Context provideContext() {
        return mActivity;
    }

    @Provides
    AppCompatActivity provideActivity() {
        return mActivity;
    }

    @Provides
    CompositeDisposable provideCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Provides
    @PerActivity
    SplashPresenter<SplashView> provideSplashPresenter(
            SplashPresenterImpl<SplashView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    HomePresenter<HomeView> provideHomePresenter(
            HomePresenterImpl<HomeView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    PopularPresenter<PopularView> providePopularPresenter(PopularPresenterImpl<PopularView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    TopRatedPresenter<TopRatedView> provideTopRatedPresenter(TopRatedPresenterImpl<TopRatedView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    UpComingPresenter<UpComingView> provideUpComingPresenter(UpComingPresenterImpl<UpComingView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    NowPlayingPresenter<NowPlayingView> provideNowPlayingPresenter(NowPlayingPresenterImpl<NowPlayingView> presenter) {
        return presenter;
    }


    @Provides
    @PerActivity
    PopularTvPresenter<PopularTvView> providePopularTvPresenter(PopularTvPresenterImpl<PopularTvView> presenter) {
        return presenter;
    }


    @Provides
    @PerActivity
    TopRatedTvPresenter<TopRatedTvView> provideTopRatedTvPresenter(TopRatedTvPresenterImpl<TopRatedTvView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    OnAirTvPresenter<OnAirTvView> provideOnAirTvPresenter(OnAirTvPresenterImpl<OnAirTvView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    AiringTodayTvPresenter<AiringTodayTvView> provideAiringTodayTvPresenter(AiringTodayTvPresenterImpl<AiringTodayTvView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    LandingPresenter<LandingView> provideLandingPresenter(LandingPresenterImpl<LandingView> presenter) {
        return presenter;
    }
    @Provides
    @PerActivity
    PopularPeoplePresenter<PopularPeopleView> providePopularPeoplePresenter(PopularPeoplePresenterImpl<PopularPeopleView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    DiscoverPresenter<DiscoverView> provideDiscoverPresenter(DiscoverPresenterImpl<DiscoverView> presenter) {
        return presenter;
    }


    @Provides
    @PerActivity
    GenresPresenter<GenresView> provideGenresPresenter(GenresPresenterImpl<GenresView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    PeopleDetailsPresenter<PeopleDetailsView> providePeopleDetailsPresenter(PeopleDetailsPresenterImpl<PeopleDetailsView> presenter){
        return presenter;
    }

    @Provides
    @PerActivity
    PeopleMovieCreditPresenter<PeopleMovieCreditView> providePeopleMovieCreditPresenter(PeopleMovieCreditPresenterImpl<PeopleMovieCreditView> presenter){
        return presenter;
    }

    @Provides
    @PerActivity
    PeopleTVShowPresenter<PeopleTVShowCreditView> providePeopleTVShowPresenter(PeopleTVShowPresenterImpl<PeopleTVShowCreditView> presenter){
        return presenter;
    }

    @Provides
    @PerActivity
    PeopleCrewPresenter<PeopleCrewView> providePeopleCrewViewPresenter(PeopleCrewPresenterImpl<PeopleCrewView> presenter){
        return presenter;
    }

    @Provides
    MoviesTVShowsAdapter provideMoviesTVShowsAdapter() {
        return new MoviesTVShowsAdapter((BaseActivity) mActivity);
    }

    @Provides
    TVShowsAdapter provideTVShowsAdapter() {
        return new TVShowsAdapter((BaseActivity) mActivity);
    }

    @Provides
    PoepleAdapter providePoepleAdapter() {
        return new PoepleAdapter((BaseActivity) mActivity);
    }

    @Provides
    PopularAdapter providePopularAdapter() {
        return new PopularAdapter((BaseActivity) mActivity);
    }


    @Provides
    TopRatedAdapter provideTopRatedAdapter() {
    return new TopRatedAdapter((BaseActivity) mActivity);
     }


    @Provides
    UpComingAdapter provideUpComingAdapter() {
        return new UpComingAdapter((BaseActivity) mActivity);
    }


    @Provides
    NowPlayingAdapter provideNowPlayingAdapter() {
        return new NowPlayingAdapter((BaseActivity) mActivity);
    }

    @Provides
    PopularTvAdapter providePopularTvAdapter() {
        return new PopularTvAdapter((BaseActivity) mActivity);
    }

    @Provides
    TopRatedTvAdapter provideTopRatedTvAdapter() {
        return new TopRatedTvAdapter((BaseActivity) mActivity);
    }


    @Provides
    OnAirTvAdapter provideOnAirTvAdapter() {
        return new OnAirTvAdapter((BaseActivity) mActivity);
    }

    @Provides
    AiringTodayTvAdapter provideAiringTodayTvAdapter() {
        return new AiringTodayTvAdapter((BaseActivity) mActivity);
    }

    @Provides
    PopularPeopleAdapter providePopularPeopleAdapter() {
        return new PopularPeopleAdapter((BaseActivity) mActivity);
    }

    @Provides
    DiscoverAdapter provideDiscoverAdapter() {
        return new DiscoverAdapter((BaseActivity) mActivity);
    }

    @Provides
    PeopleMovieCreditAdapter providePeopleMovieCreditAdapter() {
        return new PeopleMovieCreditAdapter((BaseActivity) mActivity);
    }

    @Provides
    PeopleTVshowsCreditAdapter provideTVshowsCreditAdapter() {
        return new PeopleTVshowsCreditAdapter((BaseActivity) mActivity);
    }

    @Provides
    PeopleCrewAdapter providePeopleCrewAdapter() {
        return new PeopleCrewAdapter((BaseActivity) mActivity);
    }

    @Provides
    GridLayoutManager provideGridLayoutManager(AppCompatActivity activity) {
        return new GridLayoutManager(activity,3);
    }

    @Provides
    LinearLayoutManager provideLinearLayoutManager(AppCompatActivity activity) {
        return new LinearLayoutManager(activity);
    }
}