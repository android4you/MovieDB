package com.android4you.movietmdb.di.component;


import com.android4you.movietmdb.di.module.ActivityModule;
import com.android4you.movietmdb.di.scope.PerActivity;
import com.android4you.movietmdb.ui.discover.DiscoverFragment;
import com.android4you.movietmdb.ui.home.HomeActivity;
import com.android4you.movietmdb.ui.landing.LandingFragment;
import com.android4you.movietmdb.ui.movies.nowplayiing.NowPlayingFragment;
import com.android4you.movietmdb.ui.movies.popular.PopularFragment;
import com.android4you.movietmdb.ui.people.PopularPeopleFragment;
import com.android4you.movietmdb.ui.people.details.PeopleDetailsActivity;
import com.android4you.movietmdb.ui.people.details.crew.PeopleCrewFragment;
import com.android4you.movietmdb.ui.people.details.movies.PeopleMoviesFragment;
import com.android4you.movietmdb.ui.people.details.tvshows.PeopleTvShowsFragment;
import com.android4you.movietmdb.ui.splash.SplashActivity;
import com.android4you.movietmdb.ui.movies.toprated.TopRatedFragment;
import com.android4you.movietmdb.ui.movies.upcoming.UpComingFragment;
import com.android4you.movietmdb.ui.tvshows.AiringToday.AiringTodayTvFragment;
import com.android4you.movietmdb.ui.tvshows.onAir.OnAirTvFragment;
import com.android4you.movietmdb.ui.tvshows.popular.PopularTvShowsFragment;
import com.android4you.movietmdb.ui.tvshows.toprated.TopRatedTvFragment;

import dagger.Component;

/**
 * Created by manu on 2/18/2018.
 */

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(SplashActivity activity);
    void inject(HomeActivity activity);
    void inject(LandingFragment fragment);
    void inject(PopularFragment fragment);
    void inject(TopRatedFragment fragment);
    void inject(UpComingFragment fragment);
    void inject(NowPlayingFragment fragment);

    void inject(PopularTvShowsFragment fragment);
    void inject(TopRatedTvFragment fragment);
    void inject(OnAirTvFragment fragment);
    void inject(AiringTodayTvFragment fragment);
    void inject(PopularPeopleFragment fragment);

    void inject(DiscoverFragment fragment);
    void inject(PeopleDetailsActivity activity);

    void inject(PeopleMoviesFragment fragment);
    void inject(PeopleTvShowsFragment fragment);
    void inject(PeopleCrewFragment fragment);
}
