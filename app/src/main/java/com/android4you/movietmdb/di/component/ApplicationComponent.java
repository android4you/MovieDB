package com.android4you.movietmdb.di.component;

import android.app.Application;
import android.content.Context;

import com.android4you.movietmdb.data.DataManager;
import com.android4you.movietmdb.di.module.ApplicationModule;
import com.android4you.movietmdb.di.scope.ApplicationContext;
import com.android4you.movietmdb.system.MyApp;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by manu on 2/18/2018.
 */

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(MyApp app);

    @ApplicationContext
    Context context();

    Application application();

    DataManager getDataManager();
}