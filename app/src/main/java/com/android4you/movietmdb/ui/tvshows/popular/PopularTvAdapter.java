package com.android4you.movietmdb.ui.tvshows.popular;

import android.view.View;
import android.view.ViewGroup;

import com.android4you.movietmdb.R;
import com.android4you.movietmdb.data.model.ResultsModel;
import com.android4you.movietmdb.data.model.TvShowResultModel;
import com.android4you.movietmdb.ui.base.BaseActivity;
import com.android4you.movietmdb.ui.base.BaseAdapter;
import com.android4you.movietmdb.ui.base.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by manu on 4/29/2018.
 */

public class PopularTvAdapter extends BaseAdapter {
    protected ArrayList<TvShowResultModel> tvShowResultModels = new ArrayList<>();
    OnResultClickListener onResultClickListener;
    private static final int LIST_ITEM = 0;
    private static final int GRID_ITEM = 1;
    boolean isSwitchView = false;
    public PopularTvAdapter(BaseActivity activity) {
        super(activity);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        if (viewType == LIST_ITEM){
            itemView = getInflater().inflate(R.layout.movie_row, parent, false);
        }else{
            itemView = getInflater().inflate(R.layout.movie_grid_row, parent, false);
        }
        return new PopularTvViewHolder(itemView,onResultClickListener);
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.bind(tvShowResultModels.get(position));
    }

    @Override
    public int getItemCount() {
        if (tvShowResultModels == null || tvShowResultModels.size() == 0) return 0;
        return tvShowResultModels.size();
    }

    @Override
    public int getItemViewType (int position) {
        if (isSwitchView){
            return LIST_ITEM;
        }else{
            return GRID_ITEM;
        }
    }

    public boolean toggleItemViewType(boolean isSwitch) {
        isSwitchView = isSwitch;
        return isSwitchView;
    }
    public void addItems(List<? extends TvShowResultModel> results) {
        if (results == null || results.size() == 0) return;
        int firstPosition = tvShowResultModels.size() == 0 ? 0 : tvShowResultModels.size() - 1;
        tvShowResultModels.addAll(results);
        notifyItemRangeChanged(firstPosition, results.size());
    }

    public void setOnItemClickListener(OnResultClickListener onResultClickListener) {
        this.onResultClickListener = onResultClickListener;
    }
    interface OnResultClickListener {
        void onTVShowtClick(TvShowResultModel showResultModel, View v);
    }

}