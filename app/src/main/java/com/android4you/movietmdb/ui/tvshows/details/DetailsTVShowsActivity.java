package com.android4you.movietmdb.ui.tvshows.details;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.TextView;

import com.android4you.movietmdb.BuildConfig;
import com.android4you.movietmdb.R;
import com.android4you.movietmdb.ui.base.BaseActivity;
import com.facebook.drawee.view.SimpleDraweeView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Manu on 4/27/2018.
 */

public class DetailsTVShowsActivity extends BaseActivity {

   @BindView(R.id.expandedImage)
   SimpleDraweeView imageView;
   @BindView(R.id.details)
   TextView textView;
   @Override
   protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
       setUnBinder(ButterKnife.bind(this));
       Uri uri = Uri.parse(BuildConfig.IMAGE_URL_SMALL +getIntent().getStringExtra("image"));
       imageView.setImageURI(uri);
       textView.setText(getIntent().getStringExtra("details"));
    }


    @Override
    protected void setUp() {

    }
}
