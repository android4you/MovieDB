package com.android4you.movietmdb.ui.movies.popular;

import com.android4you.movietmdb.data.model.MovieModel;
import com.android4you.movietmdb.ui.base.MvpView;

/**
 * Created by Manu on 4/28/2018.
 */

public interface PopularView extends MvpView {
    void onGettingPopularList(MovieModel movieModelList);
}
