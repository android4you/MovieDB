package com.android4you.movietmdb.ui.people.details.movies;

import com.android4you.movietmdb.ui.base.MvpPresenter;

/**
 * Created by manu on 7/11/2018.
 */

public interface PeopleMovieCreditPresenter <V extends PeopleMovieCreditView> extends MvpPresenter<V> {
}
