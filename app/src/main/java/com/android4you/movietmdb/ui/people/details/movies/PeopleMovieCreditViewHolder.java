package com.android4you.movietmdb.ui.people.details.movies;

import android.net.Uri;
import android.view.View;
import android.widget.TextView;

import com.android4you.movietmdb.BuildConfig;
import com.android4you.movietmdb.R;
import com.android4you.movietmdb.data.model.people.MovieCreditsModel;
import com.android4you.movietmdb.ui.base.BaseViewHolder;
import com.facebook.drawee.view.SimpleDraweeView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by manu on 7/11/2018.
 */

public class PeopleMovieCreditViewHolder extends BaseViewHolder {
    PeopleMovieCreditAdapter.OnMovieCreditClickListener onMovieCreditClickListener;

    View itemView;

    @BindView(R.id.my_image_view)
    SimpleDraweeView peopleProfileImage;

    @BindView(R.id.titleTV)
    TextView titleTV;

    MovieCreditsModel.CastBean castBean;


    public PeopleMovieCreditViewHolder(View itemView, PeopleMovieCreditAdapter.OnMovieCreditClickListener onMovieCreditClickListener) {
        super(itemView);
        this.itemView = itemView;
        this.onMovieCreditClickListener = onMovieCreditClickListener;
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void bind(Object model) {
        castBean = (MovieCreditsModel.CastBean)model;
        Uri uri = Uri.parse(BuildConfig.IMAGE_URL_SMALL +castBean.getPoster_path());
        peopleProfileImage.setImageURI(uri);
        titleTV.setText(castBean.getTitle());
    }

    // @OnClick(R.id.card_view)
    public void onMovieClick() {
        if (onMovieCreditClickListener != null) {
            onMovieCreditClickListener.onMovieClick(castBean, itemView);
        }
    }
}
