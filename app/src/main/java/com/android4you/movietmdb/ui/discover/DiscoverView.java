package com.android4you.movietmdb.ui.discover;

import com.android4you.movietmdb.data.model.MovieModel;
import com.android4you.movietmdb.ui.base.MvpView;

/**
 * Created by manu on 6/18/2018.
 */

public interface DiscoverView extends MvpView {

    void onGettingDiscoverList(MovieModel movieModelList);
}
