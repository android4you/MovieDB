package com.android4you.movietmdb.ui.people.details.movies;

import com.android4you.movietmdb.data.DataManager;
import com.android4you.movietmdb.ui.base.BasePresenter;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by manu on 7/11/2018.
 */

public class PeopleMovieCreditPresenterImpl<V extends PeopleMovieCreditView> extends BasePresenter<V> implements PeopleMovieCreditPresenter<V> {

    @Inject
    public PeopleMovieCreditPresenterImpl(DataManager controller, CompositeDisposable compositeDisposable) {
        super(controller, compositeDisposable);
    }

}