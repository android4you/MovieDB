package com.android4you.movietmdb.ui.movies;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android4you.movietmdb.R;
import com.android4you.movietmdb.ui.base.BaseFragment;
import com.android4you.movietmdb.ui.home.HomeActivity;
import com.android4you.movietmdb.ui.movies.nowplayiing.NowPlayingFragment;
import com.android4you.movietmdb.ui.movies.popular.PopularFragment;
import com.android4you.movietmdb.ui.movies.toprated.TopRatedFragment;
import com.android4you.movietmdb.ui.movies.upcoming.UpComingFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Manu on 4/28/2018.
 */

public class MoviesFragment extends BaseFragment {

    private View rootView;
    @BindView(R.id.tabs)
    TabLayout tabLayout;
    @BindView(R.id.viewpager)
    ViewPager viewPager;
    HomeActivity homeActivity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if(rootView!=null)
            return rootView;
        rootView = inflater.inflate(R.layout.fragment_dash_movies, container,false);

        homeActivity = (HomeActivity)getActivity();
        ButterKnife.bind(this, rootView);
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);


        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        homeActivity.headerTitle(getString(R.string.movies));
    }

    @Override
    protected void setUp(View view) {
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager());
        adapter.addFrag(new PopularFragment(), getString(R.string.popular));
        adapter.addFrag(new TopRatedFragment(), getString(R.string.toprated));
        adapter.addFrag(new NowPlayingFragment(), getString(R.string.nowplaying));
        adapter.addFrag(new UpComingFragment(), getString(R.string.upcoming));
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(4);
    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
