package com.android4you.movietmdb.ui.people.details.tvshows;

import com.android4you.movietmdb.ui.base.MvpPresenter;

/**
 * Created by manu on 7/11/2018.
 */

public interface PeopleTVShowPresenter <V extends PeopleTVShowCreditView> extends MvpPresenter<V> {
}
