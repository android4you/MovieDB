package com.android4you.movietmdb.ui.people;

import com.android4you.movietmdb.data.model.PopularPeopleModel;
import com.android4you.movietmdb.ui.base.MvpView;

/**
 * Created by manu on 5/23/2018.
 */

public interface PopularPeopleView extends MvpView {

    void onGettingPopularPeople(PopularPeopleModel peopleModel);
}
