package com.android4you.movietmdb.ui.people.details.tvshows;

import com.android4you.movietmdb.data.DataManager;
import com.android4you.movietmdb.ui.base.BasePresenter;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by manu on 7/11/2018.
 */

public class PeopleTVShowPresenterImpl<V extends PeopleTVShowCreditView> extends BasePresenter<V> implements PeopleTVShowPresenter<V> {

    @Inject
    public PeopleTVShowPresenterImpl(DataManager controller, CompositeDisposable compositeDisposable) {
        super(controller, compositeDisposable);
    }

}