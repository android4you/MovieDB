package com.android4you.movietmdb.ui.genres;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android4you.movietmdb.R;
import com.android4you.movietmdb.ui.base.BaseFragment;
import com.android4you.movietmdb.ui.home.HomeActivity;

/**
 * Created by manu on 6/18/2018.
 */

public class GenresFragment extends BaseFragment implements GenresView {

    HomeActivity homeActivity;
    @Override
    protected void setUp(View view) {

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       View rootView = inflater.inflate(R.layout.fragment_discover,container,false);
       homeActivity = (HomeActivity)getActivity();
        return rootView;
    }


    @Override
    public void onResume() {
        super.onResume();
        homeActivity.headerTitle(getString(R.string.genre));
    }
}
