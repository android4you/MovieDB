package com.android4you.movietmdb.ui.home;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.android4you.movietmdb.R;
import com.android4you.movietmdb.system.MyApp;
import com.android4you.movietmdb.ui.base.BaseActivity;
import com.android4you.movietmdb.ui.base.BaseFragment;
import com.android4you.movietmdb.ui.discover.DiscoverFragment;
import com.android4you.movietmdb.ui.genres.GenresFragment;
import com.android4you.movietmdb.ui.landing.LandingFragment;
import com.android4you.movietmdb.ui.movies.MoviesFragment;
import com.android4you.movietmdb.ui.people.PopularPeopleFragment;
import com.android4you.movietmdb.ui.tvshows.TVShowsFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by manu on 2/18/2018.
 */

public class HomeActivity extends BaseActivity implements HomeView {

    @Inject
    HomePresenter<HomeView> mPresenter;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.drawer_view)
    DrawerLayout mDrawer;

    @BindView(R.id.navigation_view)
    NavigationView mNavigationView;

    private ActionBarDrawerToggle mDrawerToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        getActivityComponent().inject(this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(this);
        setUp();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mDrawer != null)
            mDrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    public void addFragment(BaseFragment fragment, int productIndex, Object obj, boolean backStack) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
      //  fragmentTransaction.disallowAddToBackStack();

        fragmentTransaction.replace(R.id.containerView, fragment, null);
        if(backStack){
            fragmentTransaction.addToBackStack(fragment.getClass().getName());
        }
        fragmentTransaction.commit();
        fragment.setProductTagIndex(productIndex,obj);
    }


    @Override
    protected void setUp() {
        setSupportActionBar(mToolbar);
        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                mDrawer,
                mToolbar,
                R.string.open_drawer,
                R.string.close_drawer) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                hideKeyboard();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };
        mDrawer.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
        setupNavMenu();
        mPresenter.onNavMenuCreated();
        mPresenter.onViewInitialized();
        addFragment(new LandingFragment(), -1, null, false);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                // TODO Something when menu item selected
                return true;

            case R.id.action_settings:
                // TODO Something when menu item selected
                item.setIcon(getIconForMenu());
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private boolean isList = true;

    private int getIconForMenu(){
        isList = !isList;
        ((MyApp) getApplication())
                .bus().send(isList);

        return isList? R.mipmap.ic_grid: R.mipmap.ic_list;
    }

    protected void setupNavMenu() {
      //  View headerLayout = mNavigationView.getHeaderView(0);
        mNavigationView.setNavigationItemSelectedListener(
                item -> {
                    mDrawer.closeDrawer(GravityCompat.START);
                    switch (item.getItemId()) {

                        case R.id.nav_item_home:
                            addFragment(new LandingFragment(),-1, null,false);
                            return true;
                        case R.id.nav_item_movies:
                            addFragment(new MoviesFragment(),-1, null,true);
                            return true;
                        case R.id.nav_item_tvshows:
                            addFragment(new TVShowsFragment(),-1, null,true);
                            return true;
                        case R.id.nav_item_popularPeople:
                            addFragment(new PopularPeopleFragment(),-1, null,true);
                            return true;
                        case R.id.nav_item_feed:
                            mPresenter.onViewInitialized();
                            return true;
                        case R.id.nav_item_discover:
                            addFragment(new DiscoverFragment(),-1,null,true);
                            return true;
                        case R.id.nav_item_genre:
                            addFragment(new GenresFragment(),-1,null,true);
                            return true;
                        case R.id.nav_item_logout:
                            return true;
                        default:
                            return false;
                    }
                });
    }

    @Override
    public void closeNavigationDrawer() {
        if (mDrawer != null) {
            mDrawer.closeDrawer(Gravity.START);
        }
    }

    @Override
    public void lockDrawer() {
        if (mDrawer != null)
            mDrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    @Override
    public void unlockDrawer() {
        if (mDrawer != null)
            mDrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }

    public void headerTitle(String title){
        mToolbar.setTitle(title);
    }

}