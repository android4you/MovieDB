package com.android4you.movietmdb.ui.people.details.info;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android4you.movietmdb.R;
import com.android4you.movietmdb.data.model.PopularPeopleModel;
import com.android4you.movietmdb.data.model.people.PeopleDetailModel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PeopleInfoFragment extends Fragment {

    @BindView(R.id.dateofbirthTV)
    TextView dateofbirthTV;

    @BindView(R.id.placeofbirthTV)
    TextView placeofbirthTV;

    @BindView(R.id.othernameTV)
    TextView othernameTV;

    @BindView(R.id.descriptionTV)
    TextView descriptionTV;

    private View rootView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (rootView != null)
            return rootView;
        rootView = inflater.inflate(R.layout.fragment_people_info, container, false);
        ButterKnife.bind(this, rootView);
        Bundle bundle = this.getArguments();
        if(bundle != null) {
          String  json = bundle.getString("RESULTBEAN");
          String details =bundle.getString("DETAILBEAN");
          Gson gson = new Gson();
          PopularPeopleModel.ResultsBean resultsBean = gson.fromJson(json, PopularPeopleModel.ResultsBean.class);
          PeopleDetailModel peopleDetailModel = gson.fromJson(details, PeopleDetailModel.class);

          dateofbirthTV.setText(peopleDetailModel.getBirthday());
          placeofbirthTV.setText(peopleDetailModel.getPlace_of_birth());
          othernameTV.setText(peopleDetailModel.getAlso_known_as().size()>0? peopleDetailModel.getAlso_known_as().get(0):"");
          descriptionTV.setText(peopleDetailModel.getBiography());

        }
        return rootView;
    }
}
