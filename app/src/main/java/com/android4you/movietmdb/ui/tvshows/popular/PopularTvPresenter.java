package com.android4you.movietmdb.ui.tvshows.popular;

import com.android4you.movietmdb.ui.base.MvpPresenter;

/**
 * Created by manu on 4/29/2018.
 */

public interface PopularTvPresenter <V extends PopularTvView> extends MvpPresenter<V> {
    void fetchPopularTvs(String page);
}
