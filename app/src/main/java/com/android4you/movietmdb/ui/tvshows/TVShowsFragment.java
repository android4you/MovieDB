package com.android4you.movietmdb.ui.tvshows;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android4you.movietmdb.R;
import com.android4you.movietmdb.ui.base.BaseFragment;
import com.android4you.movietmdb.ui.home.HomeActivity;
import com.android4you.movietmdb.ui.tvshows.AiringToday.AiringTodayTvFragment;
import com.android4you.movietmdb.ui.tvshows.onAir.OnAirTvFragment;
import com.android4you.movietmdb.ui.tvshows.popular.PopularTvShowsFragment;
import com.android4you.movietmdb.ui.tvshows.toprated.TopRatedTvFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by manu on 4/29/2018.
 */

public class TVShowsFragment extends BaseFragment {

    private View rootView;
    @BindView(R.id.tabs)
    TabLayout tabLayout;
    @BindView(R.id.viewpager)
    ViewPager viewPager;

    HomeActivity homeActivity;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if(rootView!=null)
            return rootView;
        rootView = inflater.inflate(R.layout.fragment_dash_movies, container,false);

        homeActivity = (HomeActivity)getActivity();
        ButterKnife.bind(this, rootView);
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        return rootView;
    }

    @Override
    protected void setUp(View view) {

    }

    @Override
    public void onResume() {
        super.onResume();

        homeActivity.headerTitle(getString(R.string.tvShows));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager());
        adapter.addFrag(new PopularTvShowsFragment(), getString(R.string.popular));
        adapter.addFrag(new TopRatedTvFragment(), getString(R.string.toprated));
        adapter.addFrag(new OnAirTvFragment(), getString(R.string.onair));
        adapter.addFrag(new AiringTodayTvFragment(), getString(R.string.airingtoday));
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<android.support.v4.app.Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public android.support.v4.app.Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(android.support.v4.app.Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
