package com.android4you.movietmdb.ui.people.details.crew;

import com.android4you.movietmdb.data.DataManager;
import com.android4you.movietmdb.ui.base.BasePresenter;
import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by manu on 7/12/2018.
 */

public class PeopleCrewPresenterImpl<V extends PeopleCrewView> extends BasePresenter<V> implements PeopleCrewPresenter<V> {

    @Inject
    public PeopleCrewPresenterImpl(DataManager controller, CompositeDisposable compositeDisposable) {
        super(controller, compositeDisposable);
    }

}