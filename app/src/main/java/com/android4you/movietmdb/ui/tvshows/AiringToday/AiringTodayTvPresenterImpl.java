package com.android4you.movietmdb.ui.tvshows.AiringToday;

import com.android4you.movietmdb.BuildConfig;
import com.android4you.movietmdb.data.DataManager;
import com.android4you.movietmdb.data.model.TVShowsModel;
import com.android4you.movietmdb.ui.base.BasePresenter;
import com.android4you.movietmdb.ui.tvshows.toprated.TopRatedTvPresenter;
import com.android4you.movietmdb.ui.tvshows.toprated.TopRatedTvView;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.schedulers.Timed;

/**
 * Created by manu on 4/29/2018.
 */

public class AiringTodayTvPresenterImpl<V extends AiringTodayTvView> extends BasePresenter<V> implements AiringTodayTvPresenter<V> {

    @Inject
    public AiringTodayTvPresenterImpl(DataManager controller, CompositeDisposable compositeDisposable) {
        super(controller, compositeDisposable);
    }

    @Override
    public void fetchAiringTodayTvs(String page) {
        //getMvpView().showLoading();

        getCompositeDisposable().add(getDataManager().getAiringToday(BuildConfig.API_KEY, page)
                .timeInterval()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(new DisposableObserver<Timed<TVShowsModel>>() {
                    @Override
                    public void onComplete() {
                    }

                    @Override
                    public void onError(Throwable error) {
                        if (error instanceof java.net.UnknownHostException) {
                            getMvpView().onError("No internet connection",2);
                        }else{
                            getMvpView().onError("Service not available",2);
                        }
                    }

                    @Override
                    public void onNext(Timed<TVShowsModel> movieModelTimed) {
                        // getMvpView().hideLoading();
                        if(movieModelTimed.value()!=null)
                            getMvpView().onGettingAiringTodayList(movieModelTimed.value());
                    }
                }));
    }
}