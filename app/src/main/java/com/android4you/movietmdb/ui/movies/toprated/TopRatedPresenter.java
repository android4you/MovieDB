package com.android4you.movietmdb.ui.movies.toprated;

import com.android4you.movietmdb.ui.base.MvpPresenter;

/**
 * Created by manu on 4/29/2018.
 */

public interface TopRatedPresenter <V extends TopRatedView> extends MvpPresenter<V> {
    void fetchTopRatedMovies(String page);
}
