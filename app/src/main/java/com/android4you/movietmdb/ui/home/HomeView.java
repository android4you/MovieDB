package com.android4you.movietmdb.ui.home;


import com.android4you.movietmdb.ui.base.MvpView;

/**
 * Created by manu on 2/18/2018.
 */

public interface HomeView extends MvpView {

    void closeNavigationDrawer();

    void lockDrawer();

    void unlockDrawer();
}