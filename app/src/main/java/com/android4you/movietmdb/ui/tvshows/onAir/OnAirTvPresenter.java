package com.android4you.movietmdb.ui.tvshows.onAir;

import com.android4you.movietmdb.ui.base.MvpPresenter;
/**
 * Created by manu on 4/29/2018.
 */

public interface OnAirTvPresenter<V extends OnAirTvView> extends MvpPresenter<V> {
    void fetchOnAirTvs(String page);
}
