package com.android4you.movietmdb.ui.discover;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android4you.movietmdb.R;
import com.android4you.movietmdb.data.model.MovieModel;
import com.android4you.movietmdb.data.model.ResultsModel;
import com.android4you.movietmdb.di.component.ActivityComponent;
import com.android4you.movietmdb.ui.base.BaseFragment;
import com.android4you.movietmdb.ui.home.HomeActivity;
import com.android4you.movietmdb.ui.movies.details.DetailsMoviesActivity;
import com.android4you.movietmdb.widgets.MaterialProgressBar;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by manu on 6/18/2018.
 */

public class DiscoverFragment extends BaseFragment implements DiscoverView, DiscoverAdapter.OnResultClickListener {

    @Inject
    DiscoverPresenter<DiscoverView> mPresenter;

    private Unbinder bind;

    @Inject
    LinearLayoutManager mLayoutManager;
    @Inject
    GridLayoutManager mGridLayoutManager;

    @BindView(R.id.popularRecyclerView)
    RecyclerView popularRecyclerView;

    @BindView(R.id.progress)
    MaterialProgressBar progress;

    @BindView(R.id.noInternetIV)
    ImageView noInternetIV;

    @Inject
    DiscoverAdapter discoverAdapter;

    private View rootView;

    @BindView(R.id.swipyrefreshlayout)
    SwipyRefreshLayout mSwipyRefreshLayout;

    HomeActivity homeActivity;
    int page = 1;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_discover,container,false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            mPresenter.onAttach(this);
        }

        homeActivity = (HomeActivity)getActivity();
        page = 1;
        bind = ButterKnife.bind(this, rootView);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        setLayoutManager(isSwitch);
        popularRecyclerView.setItemAnimator(new DefaultItemAnimator());
        popularRecyclerView.setAdapter(discoverAdapter);
        discoverAdapter.setOnItemClickListener(this);
        mPresenter.fetchDiscoverMovies(page+"");
        mSwipyRefreshLayout.setOnRefreshListener(direction -> {
            page++;
            mPresenter.fetchDiscoverMovies(page+"");
        });
        return rootView;
    }

    public void setLayoutManager(boolean  aBoolean){
        if(popularRecyclerView!=null) {
            discoverAdapter.toggleItemViewType(aBoolean);
            popularRecyclerView.setLayoutManager(aBoolean ? mLayoutManager:mGridLayoutManager);
        }
    }

    @Override
    protected void setUp(View view) {
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mPresenter.onDetach();
        bind.unbind();
    }

    @Override
    public void onResume() {
        super.onResume();
        homeActivity.headerTitle(getString(R.string.discover));
    }

    @Override
    public void onGettingDiscoverList(MovieModel movieModelList) {
        if(movieModelList!=null) {
            mSwipyRefreshLayout.setVisibility(View.VISIBLE);
            progress.setVisibility(View.GONE);
            mSwipyRefreshLayout.setRefreshing(false);
            discoverAdapter.addItems(movieModelList.getResults());
        }
    }

    @Override
    public void onError(String message, int type) {
        progress.setVisibility(View.GONE);
        noInternetIV.setVisibility(View.VISIBLE);
    }

    @Override
    public void onMovieClick(ResultsModel resultsModel, View v) {
        Intent intent =new Intent(getActivity(), DetailsMoviesActivity.class);
        intent.putExtra("image",resultsModel.getBackdrop_path());
        intent.putExtra("details", resultsModel.getOverview());
        startActivity(intent);
    }
}
