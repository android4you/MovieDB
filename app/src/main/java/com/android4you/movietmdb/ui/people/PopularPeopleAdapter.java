package com.android4you.movietmdb.ui.people;

import android.view.View;
import android.view.ViewGroup;

import com.android4you.movietmdb.R;
import com.android4you.movietmdb.data.model.PopularPeopleModel;
import com.android4you.movietmdb.ui.base.BaseActivity;
import com.android4you.movietmdb.ui.base.BaseAdapter;
import com.android4you.movietmdb.ui.base.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by manu on 5/23/2018.
 */

public class PopularPeopleAdapter extends BaseAdapter {
    OnProductClickListener onProductClickListener;
    protected ArrayList<PopularPeopleModel.ResultsBean> productModelArrayList = new ArrayList<>();
    public PopularPeopleAdapter(BaseActivity activity) {
        super(activity);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PopularPeopleViewHolder(getInflater().inflate(R.layout.popularpeople_row, parent, false),onProductClickListener);
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.bind(productModelArrayList.get(position));
    }

    @Override
    public int getItemCount() {
        if (productModelArrayList == null || productModelArrayList.size() == 0) return 0;
        return productModelArrayList.size();
    }
    public void addItems(List<? extends PopularPeopleModel.ResultsBean> results) {
        if (results == null || results.size() == 0) return;
        int firstPosition = productModelArrayList.size() == 0 ? 0 : productModelArrayList.size() - 1;
        productModelArrayList.addAll(results);
        notifyItemRangeChanged(firstPosition, results.size());
    }

    public void setOnItemClickListener(OnProductClickListener onProductClickListener) {
        this.onProductClickListener = onProductClickListener;
    }
    interface OnProductClickListener {
        void onProductClick(PopularPeopleModel.ResultsBean resultsBean, View v);
    }
}
