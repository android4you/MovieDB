package com.android4you.movietmdb.ui.people.details;

import com.android4you.movietmdb.BuildConfig;
import com.android4you.movietmdb.data.DataManager;
import com.android4you.movietmdb.data.model.CombinationModel;
import com.android4you.movietmdb.data.model.DetailCombinedModel;
import com.android4you.movietmdb.data.model.PopularPeopleModel;
import com.android4you.movietmdb.data.model.people.PeopleDetailModel;
import com.android4you.movietmdb.ui.base.BasePresenter;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.schedulers.Timed;

public class PeopleDetailsPresenterImpl<V extends PeopleDetailsView> extends BasePresenter<V> implements PeopleDetailsPresenter<V> {

    @Inject
    public PeopleDetailsPresenterImpl(DataManager controller, CompositeDisposable compositeDisposable) {
        super(controller, compositeDisposable);
    }

    @Override
    public void fetchPersonDetailInfo(int id) {


        Observable<DetailCombinedModel> combined = Observable.zip(getDataManager().getPerson(id+"",BuildConfig.API_KEY),
                getDataManager().getPersonMovies(id+"",BuildConfig.API_KEY),
                getDataManager().getPersonTVShows(id+"",BuildConfig.API_KEY),
                (popular, upcoming, nowplaying) -> new DetailCombinedModel(popular, upcoming, nowplaying));


        getCompositeDisposable().add(combined
                .timeInterval()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(new DisposableObserver<Timed<DetailCombinedModel>>() {
                    @Override
                    public void onComplete() {
                    }

                    @Override
                    public void onError(Throwable error) {
                        if (error instanceof java.net.UnknownHostException) {
                            getMvpView().onError("No internet connection",2);
                        }else{
                            getMvpView().onError("Service not available",2);
                        }
                    }

                    @Override
                    public void onNext(Timed<DetailCombinedModel> movieModelTimed) {
                        // getMvpView().hideLoading();
                        if(movieModelTimed.value()!=null) {
                            getMvpView().onGettingPeopleDetailsInfo(movieModelTimed.value());
                        }
                    }
                }));
    }
}
