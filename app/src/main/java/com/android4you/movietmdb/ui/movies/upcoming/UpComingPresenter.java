package com.android4you.movietmdb.ui.movies.upcoming;

import com.android4you.movietmdb.ui.base.MvpPresenter;

/**
 * Created by manu on 4/29/2018.
 */

public interface UpComingPresenter<V extends UpComingView> extends MvpPresenter<V> {
    void fetchUpComingMovies(String page);
}
