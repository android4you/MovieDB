package com.android4you.movietmdb.ui.movies.nowplayiing;

import android.net.Uri;
import android.view.View;
import android.widget.TextView;

import com.android4you.movietmdb.BuildConfig;
import com.android4you.movietmdb.R;
import com.android4you.movietmdb.data.model.ResultsModel;
import com.android4you.movietmdb.ui.base.BaseViewHolder;
import com.facebook.drawee.view.SimpleDraweeView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by manu on 4/29/2018.
 */

public class NowPlayingVewHolder extends BaseViewHolder {

    NowPlayingAdapter.OnResultClickListener onResultClickListener;

    View itemView;

    @BindView(R.id.my_image_view)
    SimpleDraweeView imageView;

    @BindView(R.id.titleTV)
    TextView titleTV;

    ResultsModel resultsModel;

    public NowPlayingVewHolder(View itemView, NowPlayingAdapter.OnResultClickListener onResultClickListener) {
        super(itemView);
        this.itemView = itemView;
        this.onResultClickListener = onResultClickListener;
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void bind(Object model) {
        resultsModel = (ResultsModel)model;
        Uri uri = Uri.parse(BuildConfig.IMAGE_URL_SMALL + resultsModel.getPoster_path());
        imageView.setImageURI(uri);
        titleTV.setText(resultsModel.getTitle());
    }

      @OnClick(R.id.movielistItemLayout)
    public void onMovieClick() {
        if (onResultClickListener != null) {
            onResultClickListener.onMovieClick(resultsModel, itemView);
        }
    }

}