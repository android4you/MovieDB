package com.android4you.movietmdb.ui.landing;

import android.view.View;
import android.view.ViewGroup;

import com.android4you.movietmdb.R;
import com.android4you.movietmdb.data.model.TvShowResultModel;
import com.android4you.movietmdb.ui.base.BaseActivity;
import com.android4you.movietmdb.ui.base.BaseAdapter;
import com.android4you.movietmdb.ui.base.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by manu on 5/22/2018.
 */

public class TVShowsAdapter extends BaseAdapter {
    OnTVShowsClickListener itemClickListener;
    protected ArrayList<TvShowResultModel> productModelArrayList = new ArrayList<>();
    public TVShowsAdapter(BaseActivity activity) {
        super(activity);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TVShowViewHolder(getInflater().inflate(R.layout.movies_tvshow_row, parent, false),itemClickListener);
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.bind(productModelArrayList.get(position));
    }

    @Override
    public int getItemCount() {
        if (productModelArrayList == null || productModelArrayList.size() == 0) return 0;
        return productModelArrayList.size();
    }
    public void addItems(List<? extends TvShowResultModel> results) {
        if (results == null || results.size() == 0) return;
        int firstPosition = productModelArrayList.size() == 0 ? 0 : productModelArrayList.size() - 1;
        productModelArrayList.addAll(results);
        notifyItemRangeChanged(firstPosition, results.size());
    }

    public void setOnItemClickListener(OnTVShowsClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }
    interface OnTVShowsClickListener {
        void onTVShowsClick(TvShowResultModel productModel, View v);
    }

}