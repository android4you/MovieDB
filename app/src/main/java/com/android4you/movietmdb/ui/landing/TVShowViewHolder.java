package com.android4you.movietmdb.ui.landing;

import android.net.Uri;
import android.view.View;
import android.widget.TextView;

import com.android4you.movietmdb.BuildConfig;
import com.android4you.movietmdb.R;
import com.android4you.movietmdb.data.model.ResultsModel;
import com.android4you.movietmdb.data.model.TvShowResultModel;
import com.android4you.movietmdb.ui.base.BaseViewHolder;
import com.facebook.drawee.view.SimpleDraweeView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by manu on 5/22/2018.
 */

public class TVShowViewHolder extends BaseViewHolder {

    TVShowsAdapter.OnTVShowsClickListener tvShowsClickListener;

    View itemView;

    @BindView(R.id.my_image_view)
    SimpleDraweeView imageView;

    @BindView(R.id.titleTV)
    TextView titleTV;

    TvShowResultModel productModel;


    public TVShowViewHolder(View itemView, TVShowsAdapter.OnTVShowsClickListener tvShowsClickListener) {
        super(itemView);
        this.itemView = itemView;
        this.tvShowsClickListener = tvShowsClickListener;
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void bind(Object model) {
        productModel = (TvShowResultModel)model;
        Uri uri = Uri.parse(BuildConfig.IMAGE_URL_SMALL +productModel.getPoster_path());
        imageView.setImageURI(uri);
        titleTV.setText(productModel.getName());
    }

     @OnClick(R.id.movielistItemLayout)
    public void onMovieClick() {
        if (tvShowsClickListener != null) {
            tvShowsClickListener.onTVShowsClick(productModel, itemView);
        }
    }

}