package com.android4you.movietmdb.ui.people.details;

import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android4you.movietmdb.BuildConfig;
import com.android4you.movietmdb.R;
import com.android4you.movietmdb.data.model.DetailCombinedModel;
import com.android4you.movietmdb.data.model.PopularPeopleModel;
import com.android4you.movietmdb.ui.base.BaseActivity;
import com.android4you.movietmdb.ui.people.details.crew.PeopleCrewFragment;
import com.android4you.movietmdb.ui.people.details.info.PeopleInfoFragment;
import com.android4you.movietmdb.ui.people.details.movies.PeopleMoviesFragment;
import com.android4you.movietmdb.ui.people.details.tvshows.PeopleTvShowsFragment;
import com.facebook.drawee.generic.RoundingParams;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;


public class PeopleDetailsActivity extends BaseActivity
        implements PeopleDetailsView, AppBarLayout.OnOffsetChangedListener {

    private static final int PERCENTAGE_TO_ANIMATE_AVATAR = 20;
    private boolean mIsAvatarShown = true;

    private int mMaxScrollSize;

    @BindView(R.id.materialup_profile_image)
    SimpleDraweeView profileIV;

    @BindView(R.id.materialup_tabs)
    TabLayout tabLayout;

    @BindView(R.id.materialup_toolbar)
    Toolbar toolbar;

    @BindView(R.id.materialup_appbar)
    AppBarLayout appbarLayout;

    @BindView(R.id.materialup_viewpager)
    ViewPager viewPager;

    @BindView(R.id.nameTV)
    TextView nameTV;

    @BindView(R.id.profile_backdropViewPager)
    ViewPager profile_backdropViewPager;

    @Inject
    PeopleDetailsPresenter<PeopleDetailsView> mPresenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_people_details);
        getActivityComponent().inject(this);
        mPresenter.onAttach(this);
        ButterKnife.bind(this);

        //set toolbar back clicklistener
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
        appbarLayout.addOnOffsetChangedListener(this);
        mMaxScrollSize = appbarLayout.getTotalScrollRange();

        setUIData();
    }

    private void setUIData(){
        //Gson String to object
        Gson gson = new Gson();
        PopularPeopleModel.ResultsBean ob = gson.fromJson(getIntent().getStringExtra("myjson"), PopularPeopleModel.ResultsBean.class);
        // PeoplePageAdapter setup
        PeoplePageAdapter peoplePageAdapter = new PeoplePageAdapter(this, ob.getKnown_for());
        profile_backdropViewPager.setAdapter(peoplePageAdapter);
        // SetProfile
        Uri uri = Uri.parse(BuildConfig.IMAGE_URL_SMALL + ob.getProfile_path());
        profileIV.setImageURI(uri);
        // setBackdrop Image
        int color = getResources().getColor(R.color.black);
        RoundingParams roundingParams = RoundingParams.fromCornersRadius(5f);
        roundingParams.setBorder(color, 3.0f);
        roundingParams.setRoundAsCircle(true);
        profileIV.getHierarchy().setRoundingParams(roundingParams);
        //set Title
        nameTV.setText(ob.getName());
        mPresenter.fetchPersonDetailInfo(ob.getId());
    }

    @Override
    protected void setUp() {

    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
        if (mMaxScrollSize == 0)
            mMaxScrollSize = appBarLayout.getTotalScrollRange();

        int percentage = (Math.abs(i)) * 100 / mMaxScrollSize;

        if (percentage >= PERCENTAGE_TO_ANIMATE_AVATAR && mIsAvatarShown) {
            mIsAvatarShown = false;

            profileIV.animate()
                    .scaleY(0).scaleX(0)
                    .setDuration(200)
                    .start();
        }

        if (percentage <= PERCENTAGE_TO_ANIMATE_AVATAR && !mIsAvatarShown) {
            mIsAvatarShown = true;

            profileIV.animate()
                    .scaleY(1).scaleX(1)
                    .start();
        }
    }

    @Override
    public void onGettingPeopleDetailsInfo(DetailCombinedModel detailCombinedModel) {
        if(detailCombinedModel!=null){
          //  progress.setVisibility(View.GONE);
          //  mSwipyRefreshLayout.setVisibility(View.VISIBLE);

            Gson gson = new Gson();
            // set Tabs
            TabsAdapter adapter = new TabsAdapter(getSupportFragmentManager());

            String detailsInfoJson = gson.toJson(detailCombinedModel.detailModel);
            Bundle bundleInfo = new Bundle();
            bundleInfo.putString("RESULTBEAN", getIntent().getStringExtra("myjson"));
            bundleInfo.putString("DETAILBEAN", detailsInfoJson);
            PeopleInfoFragment peopleInfoFragment = new PeopleInfoFragment();
            peopleInfoFragment.setArguments(bundleInfo);
            adapter.addFrag(peopleInfoFragment, "Info");

            String detailsMoviesJson = gson.toJson(detailCombinedModel.movieCreditsModel);
            Bundle bundleMovies = new Bundle();
            bundleMovies.putString("RESULTBEAN", getIntent().getStringExtra("myjson"));
            bundleMovies.putString("DETAILBEAN", detailsMoviesJson);
            PeopleMoviesFragment peopleMoviesFragment = new PeopleMoviesFragment();
            peopleMoviesFragment.setArguments(bundleMovies);
            adapter.addFrag(peopleMoviesFragment, "Movies");

            String detailsTvShowsJson = gson.toJson(detailCombinedModel.tvShowsCreditsModel);
            Bundle bundleTvShows = new Bundle();
            bundleTvShows.putString("RESULTBEAN", getIntent().getStringExtra("myjson"));
            bundleTvShows.putString("DETAILBEAN", detailsTvShowsJson);
            PeopleTvShowsFragment peopleTvShowsFragment = new PeopleTvShowsFragment();
            peopleTvShowsFragment.setArguments(bundleTvShows);
            adapter.addFrag(peopleTvShowsFragment, "Tv Shows");

            String detailsCrewMovieJson = gson.toJson(detailCombinedModel.movieCreditsModel);
            String detailsCrewTvShowJson = gson.toJson(detailCombinedModel.tvShowsCreditsModel);
            Bundle bundleCrew = new Bundle();
            bundleCrew.putString("RESULTBEAN", getIntent().getStringExtra("myjson"));
            bundleCrew.putString("MOVIECREW", detailsCrewMovieJson);
            bundleCrew.putString("TVSHOWCREW", detailsCrewTvShowJson);
            PeopleCrewFragment peopleCrewFragment = new PeopleCrewFragment();
            peopleCrewFragment.setArguments(bundleCrew);
            adapter.addFrag(peopleCrewFragment, "Crew");

            viewPager.setAdapter(adapter);
            tabLayout.setupWithViewPager(viewPager);
            viewPager.setOffscreenPageLimit(4);
        }
    }

    private class TabsAdapter extends FragmentPagerAdapter {


        private final List<String> mFragmentTitleList = new ArrayList<>();
        private final List<Fragment> mFragmentList = new ArrayList<>();
        TabsAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        @Override
        public Fragment getItem(int i) {
            return mFragmentList.get(i);
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
