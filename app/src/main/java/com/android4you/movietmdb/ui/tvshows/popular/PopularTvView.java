package com.android4you.movietmdb.ui.tvshows.popular;


import com.android4you.movietmdb.data.model.TVShowsModel;
import com.android4you.movietmdb.ui.base.MvpView;

/**
 * Created by manu on 4/29/2018.
 */

public interface PopularTvView extends MvpView {
    void onGettingPopularList(TVShowsModel movieModelList);
}
