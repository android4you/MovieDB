package com.android4you.movietmdb.ui.home;


import com.android4you.movietmdb.di.scope.PerActivity;
import com.android4you.movietmdb.ui.base.MvpPresenter;

/**
 * Created by manu on 2/18/2018.
 */




@PerActivity
public interface HomePresenter<V extends HomeView> extends MvpPresenter<V> {

    void onDrawerOptionAboutClick();

    void onDrawerOptionTestClick();

    void onViewInitialized();

    void onNavMenuCreated();
}