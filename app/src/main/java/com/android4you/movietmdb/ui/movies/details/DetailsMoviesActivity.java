package com.android4you.movietmdb.ui.movies.details;

import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.android4you.movietmdb.BuildConfig;
import com.android4you.movietmdb.R;
import com.android4you.movietmdb.ui.base.BaseActivity;
import com.android4you.movietmdb.ui.movies.details.info.InfoFragment;
import com.android4you.movietmdb.ui.movies.details.review.ReviewFragment;
import com.android4you.movietmdb.widgets.EnhancedWrapContentViewPager;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Manu on 4/27/2018.
 */

public class DetailsMoviesActivity extends BaseActivity {

   @BindView(R.id.expandedImage)
   SimpleDraweeView imageView;
   @BindView(R.id.details)
   TextView textView;
   @BindView(R.id.toolbar)
    Toolbar toolbar;

    private TabLayout tabLayout;
    private EnhancedWrapContentViewPager viewPager;

   @Override
   protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

       setUnBinder(ButterKnife.bind(this));
       Uri uri = Uri.parse(BuildConfig.IMAGE_URL_SMALL +getIntent().getStringExtra("image"));
       imageView.setImageURI(uri);
       textView.setText(getIntent().getStringExtra("details"));
       viewPager =  findViewById(R.id.viewpager);
       setupViewPager(viewPager);
       tabLayout =  findViewById(R.id.tabs);
       tabLayout.setupWithViewPager(viewPager);
   }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new InfoFragment(), "Info");
        adapter.addFragment(new ReviewFragment(), "Cast");
        adapter.addFragment(new ReviewFragment(), "Crews");
        adapter.addFragment(new ReviewFragment(), "Seasons");
        adapter.addFragment(new ReviewFragment(), "Guide");
        adapter.addFragment(new ReviewFragment(), "Comments");
        adapter.addFragment(new ReviewFragment(), "Streaming");
        adapter.addFragment(new ReviewFragment(), "Related");
        adapter.addFragment(new ReviewFragment(), "Similiar");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    @Override
    protected void setUp() {

    }
}
