package com.android4you.movietmdb.ui.people.details;

import com.android4you.movietmdb.ui.base.MvpPresenter;

public interface PeopleDetailsPresenter<V extends PeopleDetailsView> extends MvpPresenter<V> {


    void fetchPersonDetailInfo(int id);
}
