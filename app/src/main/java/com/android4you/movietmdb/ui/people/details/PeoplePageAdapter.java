package com.android4you.movietmdb.ui.people.details;

import android.content.Context;
import android.net.Uri;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.android4you.movietmdb.BuildConfig;
import com.android4you.movietmdb.R;
import com.android4you.movietmdb.data.model.PopularPeopleModel;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

public class PeoplePageAdapter extends PagerAdapter {

    Context mContext;
    LayoutInflater mLayoutInflater;
    List<PopularPeopleModel.ResultsBean.KnownForBean> knownForBeans;

    public PeoplePageAdapter(Context context, List<PopularPeopleModel.ResultsBean.KnownForBean> knownForBeans) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.knownForBeans = knownForBeans;
    }

    @Override
    public int getCount() {
        return knownForBeans.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.pager_item, container, false);
        SimpleDraweeView backdropIV =  itemView.findViewById(R.id.imageView);
        Uri urid = Uri.parse(BuildConfig.IMAGE_URL_SMALL + knownForBeans.get(position).getBackdrop_path());
        backdropIV.setImageURI(urid);
        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}