package com.android4you.movietmdb.ui.genres;

import com.android4you.movietmdb.ui.base.MvpPresenter;

/**
 * Created by manu on 6/28/2018.
 */

public interface GenresPresenter <V extends GenresView> extends MvpPresenter<V> {
    void fetchGenresrMovies(String page);
}
