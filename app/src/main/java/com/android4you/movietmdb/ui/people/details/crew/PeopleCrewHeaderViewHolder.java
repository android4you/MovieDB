package com.android4you.movietmdb.ui.people.details.crew;

import android.view.View;
import android.widget.TextView;

import com.android4you.movietmdb.R;
import com.android4you.movietmdb.ui.base.BaseViewHolder;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by manu on 7/12/2018.
 */

public class PeopleCrewHeaderViewHolder extends BaseViewHolder {

    @BindView(R.id.headerTV)
    TextView headerTV;

    View itemView;

    public PeopleCrewHeaderViewHolder(View itemView) {
        super(itemView);
        this.itemView = itemView;

        ButterKnife.bind(this, itemView);
    }

    @Override
    public void bind(Object model) {
     String s = model.toString();
        headerTV.setText(s);
    }
}
