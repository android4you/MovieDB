package com.android4you.movietmdb.ui.people.details.crew;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android4you.movietmdb.R;
import com.android4you.movietmdb.data.model.people.MovieCreditsModel;
import com.android4you.movietmdb.data.model.people.TvShowsCreditsModel;
import com.android4you.movietmdb.ui.base.BaseActivity;
import com.android4you.movietmdb.ui.base.BaseAdapter;
import com.android4you.movietmdb.ui.base.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by manu on 7/12/2018.
 */

public class PeopleCrewAdapter extends BaseAdapter {

    protected ArrayList<Object> objectArrayList = new ArrayList<>();

    public PeopleCrewAdapter(BaseActivity activity) {
        super(activity);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case 0: {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.header_layout, parent, false);
                return new PeopleCrewHeaderViewHolder(view);
            }
            case 1: {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.move_tvshow_cast_row_layout, parent, false);
                return new PeopleCrewViewMovieHolder(view);
            }
            default: {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.move_tvshow_cast_row_layout, parent, false);
                return new PeopleCrewViewTvShowHolder(view);
            }
        }
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        if (holder instanceof PeopleCrewHeaderViewHolder) {
            holder.bind (objectArrayList.get(position));
        } else if (holder instanceof PeopleCrewViewMovieHolder) {
             holder.bind(objectArrayList.get(position));
        } else if (holder instanceof PeopleCrewViewTvShowHolder) {
            holder.bind(objectArrayList.get(position));
        }
    }

    @Override
    public int getItemCount() {
        Log.e("Size ==== > ",""+objectArrayList.size());
        return objectArrayList.size();
    }
    @Override
    public int getItemViewType (int position) {

            if (objectArrayList != null) {
                if (objectArrayList.get(position) instanceof String) {
                    return 0;
                } else if (objectArrayList.get(position) instanceof MovieCreditsModel) {
                    return 1;
                } else if (objectArrayList.get(position) instanceof TvShowsCreditsModel) {
                    return 2;
                }
            }
            return 0;

    }

    public void addItems(List<? extends Object> results) {
        if (results == null || results.size() == 0) return;
        int firstPosition = objectArrayList.size() == 0 ? 0 : objectArrayList.size() - 1;
        objectArrayList.addAll(results);
        notifyItemRangeChanged(firstPosition, results.size());
    }

}
