package com.android4you.movietmdb.ui.people.details.crew;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android4you.movietmdb.R;
import com.android4you.movietmdb.data.model.PopularPeopleModel;
import com.android4you.movietmdb.data.model.people.MovieCreditsModel;
import com.android4you.movietmdb.data.model.people.TvShowsCreditsModel;
import com.android4you.movietmdb.di.component.ActivityComponent;
import com.android4you.movietmdb.ui.base.BaseFragment;
import com.android4you.movietmdb.ui.people.details.movies.PeopleMovieCreditAdapter;
import com.android4you.movietmdb.ui.people.details.movies.PeopleMovieCreditPresenter;
import com.android4you.movietmdb.ui.people.details.movies.PeopleMovieCreditView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PeopleCrewFragment extends BaseFragment implements PeopleCrewView {


    @BindView(R.id.movieCreditsRV)
    RecyclerView movieCreditsRV;

    private View rootView;
    @Inject
    PeopleCrewPresenter<PeopleCrewView> mPresenter;

    @Inject
    PeopleCrewAdapter peopleCrewAdapter;

    @Inject
    LinearLayoutManager mLayoutManager;

    @Inject
    GridLayoutManager mGridLayoutManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (rootView != null)
            return rootView;
        rootView = inflater.inflate(R.layout.fragment_people_movies, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            mPresenter.onAttach(this);
        }
        ButterKnife.bind(this, rootView);
        Bundle bundle = this.getArguments();
        if(bundle != null) {
            String  json = bundle.getString("RESULTBEAN");
            String detailsMovieCrew =bundle.getString("MOVIECREW");
            String detailsTVShowCrew =bundle.getString("TVSHOWCREW");
            Gson gson = new Gson();

            PopularPeopleModel.ResultsBean resultsBean = gson.fromJson(json, PopularPeopleModel.ResultsBean.class);
            MovieCreditsModel movieCreditsModel = gson.fromJson(detailsMovieCrew,MovieCreditsModel.class);
            TvShowsCreditsModel tvShowsCreditsModel = gson.fromJson(detailsTVShowCrew, TvShowsCreditsModel.class);
            mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            movieCreditsRV.setLayoutManager(mLayoutManager);
          //  setLayoutManager(isSwitch);
            movieCreditsRV.setItemAnimator(new DefaultItemAnimator());
            movieCreditsRV.setAdapter(peopleCrewAdapter);

            new Handler(Looper.getMainLooper()).post(() -> {
                Log.e("UI thread", "I am the UI thread");
                List<Object> objectList = new ArrayList();
                objectList.add("Movies");
                Log.e("Size movie crew", movieCreditsModel.getCrew().size()+"");
                objectList.addAll(movieCreditsModel.getCrew());
                objectList.add("TV Shows");
                Log.e("Size tvshows crew", tvShowsCreditsModel.getCrew().size()+"");
                objectList.addAll(tvShowsCreditsModel.getCrew());
                peopleCrewAdapter.addItems(objectList);

            });
        }
        return rootView;
    }

    public void setLayoutManager(boolean  aBoolean){
        if(movieCreditsRV!=null) {
          //  peopleCrewAdapter.toggleItemViewType(aBoolean);
           // movieCreditsRV.setLayoutManager(aBoolean ? mLayoutManager:mGridLayoutManager);
        }
    }

    @Override
    protected void setUp(View view) {

    }
}
