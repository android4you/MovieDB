package com.android4you.movietmdb.ui.people.details;


import com.android4you.movietmdb.data.model.DetailCombinedModel;
import com.android4you.movietmdb.ui.base.MvpView;

public interface PeopleDetailsView extends MvpView {

    void onGettingPeopleDetailsInfo(DetailCombinedModel detailCombinedModel);
}
