package com.android4you.movietmdb.ui.movies.nowplayiing;

import com.android4you.movietmdb.data.model.MovieModel;
import com.android4you.movietmdb.ui.base.MvpView;

/**
 * Created by manu on 4/29/2018.
 */

public interface NowPlayingView extends MvpView {
    void onGettingNowPlayingList(MovieModel movieModelList);
}
