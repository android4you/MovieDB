package com.android4you.movietmdb.ui.landing;


import com.android4you.movietmdb.data.model.CombinationModel;
import com.android4you.movietmdb.data.model.PopularPeopleModel;
import com.android4you.movietmdb.data.model.TvShowsCombinationModel;
import com.android4you.movietmdb.ui.base.MvpView;

/**
 * Created by manu on 2/21/2018.
 */

public interface LandingView extends MvpView {
    void onGettingPopularList(CombinationModel movieModelList);
    void onGettingTVList(TvShowsCombinationModel movieModelList);
    void onGettingPopularPeople(PopularPeopleModel peopleModel);
}
