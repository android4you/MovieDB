package com.android4you.movietmdb.ui.landing;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android4you.movietmdb.R;
import com.android4you.movietmdb.data.model.CombinationModel;
import com.android4you.movietmdb.data.model.PopularPeopleModel;
import com.android4you.movietmdb.data.model.ResultsModel;
import com.android4you.movietmdb.data.model.TvShowResultModel;
import com.android4you.movietmdb.data.model.TvShowsCombinationModel;
import com.android4you.movietmdb.di.component.ActivityComponent;
import com.android4you.movietmdb.ui.base.BaseFragment;
import com.android4you.movietmdb.ui.home.HomeActivity;
import com.android4you.movietmdb.ui.movies.details.DetailsMoviesActivity;
import com.android4you.movietmdb.ui.tvshows.details.DetailsTVShowsActivity;
import com.android4you.movietmdb.widgets.MaterialProgressBar;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by manu on 2/21/2018.
 */

public class LandingFragment extends BaseFragment implements LandingView,
        MoviesTVShowsAdapter.OnMovieClickListener, TVShowsAdapter.OnTVShowsClickListener {

    @Inject
    LandingPresenter<LandingView> mPresenter;

    private HomeActivity homeActivity;

    private View rootView;

    @BindView(R.id.upcomingRecyclerView)
    RecyclerView upcomingRecyclerView;

    @BindView(R.id.moviesRecyclerView)
    RecyclerView moviesRecyclerView;

    @BindView(R.id.nowplayingRecyclerView)
    RecyclerView nowplayingRecyclerView;

    @BindView(R.id.topRatedRecyclerView)
    RecyclerView topRatedRecyclerView;

    @BindView(R.id.popularTVRecyclerView)
    RecyclerView populatTVRecyclerView;

    @BindView(R.id.topRatedTVRecyclerView)
    RecyclerView topRatedTVRecyclerView;

    @BindView(R.id.onAirRecyclerView)
    RecyclerView onAirTVRecyclerView;

    @BindView(R.id.airingRecyclerView)
    RecyclerView airingTVRecyclerView;

    @BindView(R.id.peopleRecyclerView)
    RecyclerView peopleRecyclerView;


    @BindView(R.id.popularProgress)
    MaterialProgressBar popularProgress;

    @BindView(R.id.upcomingProgress)
    MaterialProgressBar upcomingProgress;

    @BindView(R.id.nowplayingProgress)
    MaterialProgressBar nowplayingProgress;

    @BindView(R.id.topratedProgress)
    MaterialProgressBar topratedProgress;

    @BindView(R.id.popularTVProgress)
    MaterialProgressBar popularTVProgress;

    @BindView(R.id.topRatedTVProgress)
    MaterialProgressBar topRatedTVProgress;

    @BindView(R.id.onAirProgress)
    MaterialProgressBar onAirProgress;

    @BindView(R.id.airingProgress)
    MaterialProgressBar airingProgress;


    @Inject
    LinearLayoutManager mLayoutManager;

    @Inject
    LinearLayoutManager nLayoutManager;

    @Inject
    LinearLayoutManager gLayoutManager;

    @Inject
    LinearLayoutManager uLayoutManager;


    @Inject
    LinearLayoutManager popularTVLayoutManager;

    @Inject
    LinearLayoutManager topRatedTVLayoutManager;

    @Inject
    LinearLayoutManager onAirTVLayoutManager;

    @Inject
    LinearLayoutManager airingTVLayoutManager;

    @Inject
    LinearLayoutManager peopleLayoutManager;

    @Inject
    MoviesTVShowsAdapter productAdapter;

    @Inject
    MoviesTVShowsAdapter upcomingAdapter;

    @Inject
    MoviesTVShowsAdapter nowPlayingAdapter;

    @Inject
    MoviesTVShowsAdapter topRatedAdapter;


    @Inject
    TVShowsAdapter popularTVAdapter;

    @Inject
    TVShowsAdapter topRatedTVAdapter;

    @Inject
    TVShowsAdapter onAirTVAdapter;

    @Inject
    TVShowsAdapter airingTVAdapter;

    @Inject
    PoepleAdapter poepleAdapter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if(rootView!=null)
            return rootView;
        rootView = inflater.inflate(R.layout.fragment_landing,container,false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            mPresenter.onAttach(this);
        }
        homeActivity = (HomeActivity)getActivity();
        ButterKnife.bind(this, rootView);

        mLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        nLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        gLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        uLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);

        popularTVLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        topRatedTVLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        onAirTVLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        airingTVLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);

        peopleLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);

        moviesRecyclerView.setLayoutManager(mLayoutManager);
        moviesRecyclerView.setItemAnimator(new DefaultItemAnimator());
        moviesRecyclerView.setAdapter(productAdapter);
        productAdapter.setOnItemClickListener(this);

        upcomingRecyclerView.setLayoutManager(nLayoutManager);
        upcomingRecyclerView.setItemAnimator(new DefaultItemAnimator());
        upcomingRecyclerView.setAdapter(upcomingAdapter);
        upcomingAdapter.setOnItemClickListener(this);

        nowplayingRecyclerView.setLayoutManager(gLayoutManager);
        nowplayingRecyclerView.setItemAnimator(new DefaultItemAnimator());
        nowplayingRecyclerView.setAdapter(nowPlayingAdapter);
        nowPlayingAdapter.setOnItemClickListener(this);

        topRatedRecyclerView.setLayoutManager(uLayoutManager);
        topRatedRecyclerView.setItemAnimator(new DefaultItemAnimator());
        topRatedRecyclerView.setAdapter(topRatedAdapter);
        topRatedAdapter.setOnItemClickListener(this);

        //Tv Shows
        populatTVRecyclerView.setLayoutManager(popularTVLayoutManager);
        populatTVRecyclerView.setItemAnimator(new DefaultItemAnimator());
        populatTVRecyclerView.setAdapter(popularTVAdapter);
        popularTVAdapter.setOnItemClickListener(this);

        topRatedTVRecyclerView.setLayoutManager(topRatedTVLayoutManager);
        topRatedTVRecyclerView.setItemAnimator(new DefaultItemAnimator());
        topRatedTVRecyclerView.setAdapter(topRatedTVAdapter);
        topRatedTVAdapter.setOnItemClickListener(this);

        onAirTVRecyclerView.setLayoutManager(onAirTVLayoutManager);
        onAirTVRecyclerView.setItemAnimator(new DefaultItemAnimator());
        onAirTVRecyclerView.setAdapter(onAirTVAdapter);
        onAirTVAdapter.setOnItemClickListener(this);

        airingTVRecyclerView.setLayoutManager(airingTVLayoutManager);
        airingTVRecyclerView.setItemAnimator(new DefaultItemAnimator());
        airingTVRecyclerView.setAdapter(airingTVAdapter);
        airingTVAdapter.setOnItemClickListener(this);

        peopleRecyclerView.setLayoutManager(peopleLayoutManager);
        peopleRecyclerView.setItemAnimator(new DefaultItemAnimator());
        peopleRecyclerView.setAdapter(poepleAdapter);

        mPresenter.fetchPopularMovies("1");
        return rootView;
    }


    @Override
    protected void setUp(View view) {
    }

    @Override
    public void onResume() {
        super.onResume();
        homeActivity.headerTitle(getString( R.string.home));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mPresenter.onDetach();

    }

    @Override
    public void onGettingPopularList(CombinationModel movieModelList) {
        if(movieModelList.popular!=null){
        if(movieModelList.popular.getResults()!=null){
            popularProgress.setVisibility(View.GONE);
            moviesRecyclerView.setVisibility(View.VISIBLE);
            productAdapter.addItems(movieModelList.popular.getResults());
        }

        }
        if(movieModelList.upcoming!=null){
            upcomingProgress.setVisibility(View.GONE);
            upcomingRecyclerView.setVisibility(View.VISIBLE);
            upcomingAdapter.addItems(movieModelList.upcoming.getResults());
        }
        if(movieModelList.nowplaying!=null){
            nowplayingProgress.setVisibility(View.GONE);
            nowplayingRecyclerView.setVisibility(View.VISIBLE);
            nowPlayingAdapter.addItems(movieModelList.nowplaying.getResults());
        }

        if(movieModelList.topRated!=null){
            topratedProgress.setVisibility(View.GONE);
            topRatedRecyclerView.setVisibility(View.VISIBLE);
            topRatedAdapter.addItems(movieModelList.topRated.getResults());
        }
    }

    @Override
    public void onGettingTVList(TvShowsCombinationModel movieModelList) {
        if(movieModelList.popular!=null){
            if(movieModelList.popular.getResults()!=null){
                popularTVProgress.setVisibility(View.GONE);
                populatTVRecyclerView.setVisibility(View.VISIBLE);
                popularTVAdapter.addItems(movieModelList.popular.getResults());
            }

        }
        if(movieModelList.topRated!=null){
            topRatedTVProgress.setVisibility(View.GONE);
            topRatedTVRecyclerView.setVisibility(View.VISIBLE);
            topRatedTVAdapter.addItems(movieModelList.topRated.getResults());
        }
        if(movieModelList.onAir!=null){
            onAirProgress.setVisibility(View.GONE);
            onAirTVRecyclerView.setVisibility(View.VISIBLE);
            onAirTVAdapter.addItems(movieModelList.onAir.getResults());
        }

        if(movieModelList.airingToday!=null){
            airingProgress.setVisibility(View.GONE);
            airingTVRecyclerView.setVisibility(View.VISIBLE);
            airingTVAdapter.addItems(movieModelList.airingToday.getResults());
        }
    }

    @Override
    public void onGettingPopularPeople(PopularPeopleModel peopleModel) {
        if(peopleModel.getResults()!=null){
//            airingProgress.setVisibility(View.GONE);
//            airingTVRecyclerView.setVisibility(View.VISIBLE);
            poepleAdapter.addItems(peopleModel.getResults());
        }
    }

    @Override
    public void onError(String message,int i) {
       if(i==1){
           popularProgress.setVisibility(View.GONE);
           upcomingProgress.setVisibility(View.GONE);
           nowplayingProgress.setVisibility(View.GONE);
           topratedProgress.setVisibility(View.GONE);
           popularTVProgress.setVisibility(View.GONE);
           topRatedTVProgress.setVisibility(View.GONE);
           onAirProgress.setVisibility(View.GONE);
           airingProgress.setVisibility(View.GONE);
       }
       else if(i==2){
           popularTVProgress.setVisibility(View.GONE);
           topRatedTVProgress.setVisibility(View.GONE);
           onAirProgress.setVisibility(View.GONE);
           airingProgress.setVisibility(View.GONE);
       }else if(i==3){

       }
    }

    @Override
    public void onMovieClick(ResultsModel resultsModel, View v) {
        Intent intent =new Intent(getActivity(), DetailsMoviesActivity.class);
        intent.putExtra("image",resultsModel.getBackdrop_path());
        intent.putExtra("details", resultsModel.getOverview());
        startActivity(intent);
    }

    @Override
    public void onTVShowsClick(TvShowResultModel showResultModel, View v) {
        Intent intent =new Intent(getActivity(), DetailsTVShowsActivity.class);
        intent.putExtra("image",showResultModel.getBackdrop_path());
        intent.putExtra("details", showResultModel.getOverview());
        startActivity(intent);
    }

//    private void testing(){
//        if (scroller != null) {
//
//            scroller.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
//                @Override
//                public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
//
//                    if (scrollY > oldScrollY) {
//                        Log.i(TAG, "Scroll DOWN");
//                    }
//                    if (scrollY < oldScrollY) {
//                        Log.i(TAG, "Scroll UP");
//                    }
//
//                    if (scrollY == 0) {
//                        Log.i(TAG, "TOP SCROLL");
//                    }
//
//                    if (scrollY == ( v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight() )) {
//                        Log.i(TAG, "BOTTOM SCROLL");
//                    }
//                }
//            });
//        }
//    }
}
