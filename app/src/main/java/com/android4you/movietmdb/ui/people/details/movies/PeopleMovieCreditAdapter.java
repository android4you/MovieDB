package com.android4you.movietmdb.ui.people.details.movies;

import android.view.View;
import android.view.ViewGroup;

import com.android4you.movietmdb.R;
import com.android4you.movietmdb.data.model.people.MovieCreditsModel;
import com.android4you.movietmdb.ui.base.BaseActivity;
import com.android4you.movietmdb.ui.base.BaseAdapter;
import com.android4you.movietmdb.ui.base.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by manu on 7/11/2018.
 */

public class PeopleMovieCreditAdapter extends BaseAdapter {

    private static final int LIST_ITEM = 0;

    private static final int GRID_ITEM = 1;

    boolean isSwitchView = false;
    OnMovieCreditClickListener itemClickListener;

    protected ArrayList<MovieCreditsModel.CastBean> castBeanArrayList = new ArrayList<>();

    public PeopleMovieCreditAdapter(BaseActivity activity) {
        super(activity);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        if (viewType == LIST_ITEM){
            itemView = getInflater().inflate(R.layout.movie_row, parent, false);
        }else{
            itemView = getInflater().inflate(R.layout.movie_grid_row, parent, false);
        }
        return new PeopleMovieCreditViewHolder(itemView,itemClickListener);
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.bind(castBeanArrayList.get(position));
    }

    @Override
    public int getItemCount() {
        if (castBeanArrayList == null || castBeanArrayList.size() == 0) return 0;
        return castBeanArrayList.size();
    }

    @Override
    public int getItemViewType (int position) {
        if (isSwitchView){
            return LIST_ITEM;
        }else{
            return GRID_ITEM;
        }
    }

    public boolean toggleItemViewType(boolean isSwitch) {
        isSwitchView = isSwitch;
        return isSwitchView;
    }
    public void addItems(List<? extends MovieCreditsModel.CastBean> results) {
        if (results == null || results.size() == 0) return;
        int firstPosition = castBeanArrayList.size() == 0 ? 0 : castBeanArrayList.size() - 1;
        castBeanArrayList.addAll(results);
        notifyItemRangeChanged(firstPosition, results.size());
    }

    public void setOnItemClickListener(OnMovieCreditClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }
    interface OnMovieCreditClickListener {
        void onMovieClick(MovieCreditsModel.CastBean castBean, View v);
    }
}
