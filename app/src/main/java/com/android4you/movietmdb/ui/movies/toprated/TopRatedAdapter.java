package com.android4you.movietmdb.ui.movies.toprated;

import android.view.View;
import android.view.ViewGroup;

import com.android4you.movietmdb.R;
import com.android4you.movietmdb.data.model.ResultsModel;
import com.android4you.movietmdb.ui.base.BaseActivity;
import com.android4you.movietmdb.ui.base.BaseAdapter;
import com.android4you.movietmdb.ui.base.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by manu on 4/29/2018.
 */

public class TopRatedAdapter extends BaseAdapter {
    protected ArrayList<ResultsModel> resultsModels = new ArrayList<>();
    OnResultClickListener onResultClickListener;
    private static final int LIST_ITEM = 0;
    private static final int GRID_ITEM = 1;
    boolean isSwitchView = false;
    public TopRatedAdapter(BaseActivity activity) {
        super(activity);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        if (viewType == LIST_ITEM){
            itemView = getInflater().inflate(R.layout.movie_row, parent, false);
        }else{
            itemView = getInflater().inflate(R.layout.movie_grid_row, parent, false);
        }
        return new TopRatedViewHolder(itemView,onResultClickListener);
    }
    @Override
    public int getItemViewType (int position) {
        if (isSwitchView){
            return LIST_ITEM;
        }else{
            return GRID_ITEM;
        }
    }

    public boolean toggleItemViewType (boolean aBoolean) {
        isSwitchView = aBoolean;
        return isSwitchView;
    }
    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.bind(resultsModels.get(position));
    }

    @Override
    public int getItemCount() {
        if (resultsModels == null || resultsModels.size() == 0) return 0;
        return resultsModels.size();
    }
    public void addItems(List<? extends ResultsModel> results) {
        if (results == null || results.size() == 0) return;
        int firstPosition = resultsModels.size() == 0 ? 0 : resultsModels.size() - 1;
        resultsModels.addAll(results);
        notifyItemRangeChanged(firstPosition, results.size());
    }

    public void setOnItemClickListener(OnResultClickListener onResultClickListener) {
        this.onResultClickListener = onResultClickListener;
    }
    interface OnResultClickListener {
        void onMovieClick(ResultsModel productModel, View v);
    }

}