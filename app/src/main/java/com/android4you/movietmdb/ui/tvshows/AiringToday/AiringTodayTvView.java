package com.android4you.movietmdb.ui.tvshows.AiringToday;

import com.android4you.movietmdb.data.model.TVShowsModel;
import com.android4you.movietmdb.ui.base.MvpView;

/**
 * Created by manu on 4/29/2018.
 */

public interface AiringTodayTvView extends MvpView {
    void onGettingAiringTodayList(TVShowsModel movieModelList);
}
