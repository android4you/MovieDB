package com.android4you.movietmdb.ui.people.details.tvshows;

import android.net.Uri;
import android.view.View;
import android.widget.TextView;

import com.android4you.movietmdb.BuildConfig;
import com.android4you.movietmdb.R;
import com.android4you.movietmdb.data.model.people.TvShowsCreditsModel;
import com.android4you.movietmdb.ui.base.BaseViewHolder;
import com.facebook.drawee.view.SimpleDraweeView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by manu on 7/11/2018.
 */

public class PeopleTVShowCreditViewHolder extends BaseViewHolder {
    PeopleTVshowsCreditAdapter.OnTVShowsCreditClickListener onMovieCreditClickListener;

    View itemView;

    @BindView(R.id.my_image_view)
    SimpleDraweeView peopleProfileImage;

    @BindView(R.id.titleTV)
    TextView titleTV;

    TvShowsCreditsModel.CastBean castBean;


    public PeopleTVShowCreditViewHolder(View itemView, PeopleTVshowsCreditAdapter.OnTVShowsCreditClickListener onMovieCreditClickListener) {
        super(itemView);
        this.itemView = itemView;
        this.onMovieCreditClickListener = onMovieCreditClickListener;
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void bind(Object model) {
        castBean = (TvShowsCreditsModel.CastBean)model;
        Uri uri = Uri.parse(BuildConfig.IMAGE_URL_SMALL +castBean.getPoster_path());
        peopleProfileImage.setImageURI(uri);
        titleTV.setText(castBean.getName());
    }

    // @OnClick(R.id.card_view)
    public void onMovieClick() {
        if (onMovieCreditClickListener != null) {
            onMovieCreditClickListener.onTVShowClick(castBean, itemView);
        }
    }
}
