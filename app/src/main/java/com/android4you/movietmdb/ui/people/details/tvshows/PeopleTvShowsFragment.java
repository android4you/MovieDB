package com.android4you.movietmdb.ui.people.details.tvshows;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android4you.movietmdb.R;
import com.android4you.movietmdb.data.model.PopularPeopleModel;
import com.android4you.movietmdb.data.model.people.MovieCreditsModel;
import com.android4you.movietmdb.data.model.people.TvShowsCreditsModel;
import com.android4you.movietmdb.di.component.ActivityComponent;
import com.android4you.movietmdb.ui.base.BaseFragment;
import com.android4you.movietmdb.ui.people.details.movies.PeopleMovieCreditAdapter;
import com.android4you.movietmdb.ui.people.details.movies.PeopleMovieCreditPresenter;
import com.android4you.movietmdb.ui.people.details.movies.PeopleMovieCreditView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PeopleTvShowsFragment extends BaseFragment implements PeopleTVShowCreditView{

    @BindView(R.id.movieCreditsRV)
    RecyclerView movieCreditsRV;

    private View rootView;
    @Inject
    PeopleTVShowPresenter<PeopleTVShowCreditView> mPresenter;
    @Inject
    PeopleTVshowsCreditAdapter peopleTVshowsCreditAdapter;
    @Inject
    LinearLayoutManager mLayoutManager;
    @Inject
    GridLayoutManager mGridLayoutManager;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        if (rootView != null)
            return rootView;
        rootView = inflater.inflate(R.layout.fragment_people_movies, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            mPresenter.onAttach(this);
        }
        ButterKnife.bind(this, rootView);
        Bundle bundle = this.getArguments();
        if(bundle != null) {
            String  json = bundle.getString("RESULTBEAN");
            String details =bundle.getString("DETAILBEAN");
            Gson gson = new Gson();

            PopularPeopleModel.ResultsBean resultsBean = gson.fromJson(json, PopularPeopleModel.ResultsBean.class);
            TvShowsCreditsModel tvShowsCreditsModel = gson.fromJson(details,TvShowsCreditsModel.class);

            mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            setLayoutManager(isSwitch);
            movieCreditsRV.setItemAnimator(new DefaultItemAnimator());
            movieCreditsRV.setAdapter(peopleTVshowsCreditAdapter);

            new Handler(Looper.getMainLooper()).post(() -> {
                Log.e("UI thread", "I am the UI thread");
                peopleTVshowsCreditAdapter.addItems(tvShowsCreditsModel.getCast());

            });
        }
        return rootView;
    }
    public void setLayoutManager(boolean  aBoolean){
        if(movieCreditsRV!=null) {
            peopleTVshowsCreditAdapter.toggleItemViewType(aBoolean);
            movieCreditsRV.setLayoutManager(aBoolean ? mLayoutManager:mGridLayoutManager);
        }
    }

    @Override
    protected void setUp(View view) {

    }
}



