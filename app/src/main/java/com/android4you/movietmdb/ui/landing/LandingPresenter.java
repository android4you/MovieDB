package com.android4you.movietmdb.ui.landing;


import com.android4you.movietmdb.di.scope.PerActivity;
import com.android4you.movietmdb.ui.base.MvpPresenter;

/**
 * Created by manu on 2/21/2018.
 */

@PerActivity
public interface LandingPresenter<V extends LandingView> extends MvpPresenter<V> {
    void fetchPopularMovies(String page);
}