package com.android4you.movietmdb.ui.tvshows.AiringToday;

import com.android4you.movietmdb.ui.base.MvpPresenter;

/**
 * Created by manu on 4/29/2018.
 */

public interface AiringTodayTvPresenter <V extends AiringTodayTvView> extends MvpPresenter<V> {
    void fetchAiringTodayTvs(String page);
}
