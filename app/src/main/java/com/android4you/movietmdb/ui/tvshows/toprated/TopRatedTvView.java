package com.android4you.movietmdb.ui.tvshows.toprated;

import com.android4you.movietmdb.data.model.TVShowsModel;
import com.android4you.movietmdb.ui.base.MvpView;

/**
 * Created by manu on 4/29/2018.
 */

public interface TopRatedTvView extends MvpView {
    void onGettingTopRatedList(TVShowsModel movieModelList);
}
