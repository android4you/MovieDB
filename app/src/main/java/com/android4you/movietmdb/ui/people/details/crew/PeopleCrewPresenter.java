package com.android4you.movietmdb.ui.people.details.crew;

import com.android4you.movietmdb.ui.base.MvpPresenter;

/**
 * Created by manu on 7/12/2018.
 */

public interface PeopleCrewPresenter <V extends PeopleCrewView> extends MvpPresenter<V> {
}
