package com.android4you.movietmdb.ui.movies.nowplayiing;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android4you.movietmdb.R;
import com.android4you.movietmdb.data.model.MovieModel;
import com.android4you.movietmdb.data.model.ResultsModel;
import com.android4you.movietmdb.di.component.ActivityComponent;
import com.android4you.movietmdb.system.MyApp;
import com.android4you.movietmdb.ui.base.BaseFragment;
import com.android4you.movietmdb.ui.movies.details.DetailsMoviesActivity;
import com.android4you.movietmdb.widgets.MaterialProgressBar;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by manu on 4/29/2018.
 */

public class NowPlayingFragment extends BaseFragment implements NowPlayingView, NowPlayingAdapter.OnResultClickListener {

    private Unbinder bind;

    @Inject
    LinearLayoutManager mLayoutManager;
    @Inject
    GridLayoutManager mGridLayoutManager;
    @BindView(R.id.popularRecyclerView)
    RecyclerView popularRecyclerView;

    @BindView(R.id.progress)
    MaterialProgressBar progress;

    @BindView(R.id.noInternetIV)
    ImageView noInternetIV;

    @Inject
    NowPlayingPresenter<NowPlayingView> mPresenter;

    @Inject
    NowPlayingAdapter popularAdapter;

    private View rootView;

    @BindView(R.id.swipyrefreshlayout)
    SwipyRefreshLayout mSwipyRefreshLayout;

    int page = 1;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_popular,container,false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            mPresenter.onAttach(this);
        }
        page = 1;
        bind = ButterKnife.bind(this, rootView);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        setLayoutManager(isSwitch);
        popularRecyclerView.setItemAnimator(new DefaultItemAnimator());
        popularRecyclerView.setAdapter(popularAdapter);
        popularAdapter.setOnItemClickListener(this);
        mPresenter.fetchNowPlayingMovies(page+"");
        mSwipyRefreshLayout.setOnRefreshListener(direction -> {
            page++;
            mPresenter.fetchNowPlayingMovies(page+"");
        });
        return rootView;
    }

    public void setLayoutManager(boolean  aBoolean){
        if(popularRecyclerView!=null) {
            popularAdapter.toggleItemViewType(aBoolean);
            popularRecyclerView.setLayoutManager(aBoolean ? mLayoutManager:mGridLayoutManager);
        }
    }

    @Override
    protected void setUp(View view) {
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mPresenter.onDetach();
        bind.unbind();
    }

    @Override
    public void onGettingNowPlayingList(MovieModel movieModelList) {
        if(movieModelList!=null) {
            mSwipyRefreshLayout.setVisibility(View.VISIBLE);
            progress.setVisibility(View.GONE);
            mSwipyRefreshLayout.setRefreshing(false);
            popularAdapter.addItems(movieModelList.getResults());
        }
    }

    @Override
    public void onError(String message, int type) {
        progress.setVisibility(View.GONE);
        noInternetIV.setVisibility(View.VISIBLE);
    }

    @Override
    public void onMovieClick(ResultsModel resultsModel, View v) {
        Intent intent =new Intent(getActivity(), DetailsMoviesActivity.class);
        intent.putExtra("image",resultsModel.getBackdrop_path());
        intent.putExtra("details", resultsModel.getOverview());
        startActivity(intent);
    }
}
