package com.android4you.movietmdb.ui.landing;



import com.android4you.movietmdb.BuildConfig;
import com.android4you.movietmdb.data.DataManager;
import com.android4you.movietmdb.data.model.CombinationModel;
import com.android4you.movietmdb.data.model.PopularPeopleModel;
import com.android4you.movietmdb.data.model.TvShowsCombinationModel;
import com.android4you.movietmdb.ui.base.BasePresenter;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.schedulers.Timed;

/**
 * Created by manu on 2/21/2018.
 */



public class LandingPresenterImpl<V extends LandingView> extends BasePresenter<V> implements LandingPresenter<V> {

    @Inject
    public LandingPresenterImpl(DataManager controller, CompositeDisposable compositeDisposable) {
        super(controller, compositeDisposable);
    }

    @Override
    public void fetchPopularMovies( String page) {
       // getMvpView().showLoading();
        Observable<CombinationModel> combined = Observable.zip(getDataManager().getPopularMovies(BuildConfig.API_KEY, page),
                getDataManager().getUpcomingMovies(BuildConfig.API_KEY, page),
                getDataManager().getNowPlayingMovies(BuildConfig.API_KEY, page),
                getDataManager().getTopRatedMovies(BuildConfig.API_KEY, page),
                (popular, upcoming, nowplaying, topRated) -> new CombinationModel(popular, upcoming, nowplaying, topRated));

        getCompositeDisposable().add(combined
                .timeInterval()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(new DisposableObserver<Timed<CombinationModel>>() {
                    @Override
                    public void onComplete() {
                    }

                    @Override
                    public void onError(Throwable error) {

                        if (error instanceof java.net.UnknownHostException) {
                            getMvpView().onError("No internet connection",1);
                        }else{
                            getMvpView().onError("Service not available",1);
                        }
                    }

                    @Override
                    public void onNext(Timed<CombinationModel> movieModelTimed) {
                      //  getMvpView().hideLoading();
                        getMvpView().onGettingPopularList(movieModelTimed.value());
                        fetchTVShows(page);
                    }
                }));


    }

    private void fetchTVShows(String page){
        //getMvpView().showLoading();
        Observable<TvShowsCombinationModel> combined = Observable.zip(getDataManager().getPopularTVs(BuildConfig.API_KEY, page),
                getDataManager().getTopRatedTVs(BuildConfig.API_KEY, page),
                getDataManager().getOnAirTVs(BuildConfig.API_KEY, page),
                getDataManager().getAiringToday(BuildConfig.API_KEY, page),
                (popular, topRated, onAir, airingToday) -> new TvShowsCombinationModel(popular, topRated, onAir, airingToday));

        getCompositeDisposable().add(combined
                .timeInterval()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(new DisposableObserver<Timed<TvShowsCombinationModel>>() {
                    @Override
                    public void onComplete() {
                    }

                    @Override
                    public void onError(Throwable error) {
                        if (error instanceof java.net.UnknownHostException) {
                            getMvpView().onError("No internet connection",2);
                        }else{
                            getMvpView().onError("Service not available",2);
                        }
                    }

                    @Override
                    public void onNext(Timed<TvShowsCombinationModel> movieModelTimed) {
                       // getMvpView().hideLoading();
                        getMvpView().onGettingTVList(movieModelTimed.value());
                        fetchPopularPeople();
                    }
                }));
    }

    private void fetchPopularPeople(){
        //getMvpView().showLoading();
        getCompositeDisposable().add(getDataManager().getpopularPeople(BuildConfig.API_KEY, "1")
                .timeInterval()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(new DisposableObserver<Timed<PopularPeopleModel>>() {
                    @Override
                    public void onComplete() {
                    }

                    @Override
                    public void onError(Throwable error) {
                        if (error instanceof java.net.UnknownHostException) {
                            getMvpView().onError("No internet connection", 3);
                        }else{
                            getMvpView().onError("Service not available", 3);
                        }
                    }

                    @Override
                    public void onNext(Timed<PopularPeopleModel> movieModelTimed) {
                        // getMvpView().hideLoading();
                        if(movieModelTimed.value()!=null)
                            getMvpView().onGettingPopularPeople(movieModelTimed.value());
                    }
                }));
    }

    @Override
    public void onDetach() {
        super.onDetach();
        getCompositeDisposable().clear();
    }
}