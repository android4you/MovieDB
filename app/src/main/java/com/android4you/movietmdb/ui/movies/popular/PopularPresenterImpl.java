package com.android4you.movietmdb.ui.movies.popular;

import com.android4you.movietmdb.BuildConfig;
import com.android4you.movietmdb.data.DataManager;
import com.android4you.movietmdb.data.model.MovieModel;
import com.android4you.movietmdb.ui.base.BasePresenter;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.schedulers.Timed;

/**
 * Created by Manu on 4/28/2018.
 */

public class PopularPresenterImpl<V extends PopularView> extends BasePresenter<V> implements PopularPresenter<V> {

    @Inject
    public PopularPresenterImpl(DataManager controller, CompositeDisposable compositeDisposable) {
        super(controller, compositeDisposable);
    }

    @Override
    public void fetchPopularMovies(String page) {
       //getMvpView().showLoading();

        getCompositeDisposable().add(getDataManager().getPopularMovies(BuildConfig.API_KEY, page)
                .timeInterval()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(new DisposableObserver<Timed<MovieModel>>() {
                    @Override
                    public void onComplete() {
                    }

                    @Override
                    public void onError(Throwable error) {
                        if (error instanceof java.net.UnknownHostException) {
                            getMvpView().onError("No internet connection",2);
                        }else{
                            getMvpView().onError("Service not available",2);
                        }
                    }

                    @Override
                    public void onNext(Timed<MovieModel> movieModelTimed) {
                       // getMvpView().hideLoading();
                        if(movieModelTimed.value()!=null)
                        getMvpView().onGettingPopularList(movieModelTimed.value());
                    }
                }));
    }
}