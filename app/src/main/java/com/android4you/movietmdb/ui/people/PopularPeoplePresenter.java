package com.android4you.movietmdb.ui.people;

import com.android4you.movietmdb.ui.base.MvpPresenter;

/**
 * Created by manu on 5/23/2018.
 */

public interface PopularPeoplePresenter<V extends PopularPeopleView> extends MvpPresenter<V> {
    void fetchPopulaPeoples(String page);
}
