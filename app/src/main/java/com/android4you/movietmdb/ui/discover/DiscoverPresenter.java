package com.android4you.movietmdb.ui.discover;

import com.android4you.movietmdb.ui.base.MvpPresenter;

/**
 * Created by manu on 6/18/2018.
 */

public interface DiscoverPresenter <V extends DiscoverView> extends MvpPresenter<V> {
    void fetchDiscoverMovies(String page);
}
