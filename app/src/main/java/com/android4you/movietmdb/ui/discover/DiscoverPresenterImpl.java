package com.android4you.movietmdb.ui.discover;

import com.android4you.movietmdb.BuildConfig;
import com.android4you.movietmdb.data.DataManager;
import com.android4you.movietmdb.data.model.MovieModel;
import com.android4you.movietmdb.ui.base.BasePresenter;
import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.schedulers.Timed;

/**
 * Created by manu on 6/18/2018.
 */

public class DiscoverPresenterImpl <V extends DiscoverView> extends BasePresenter<V>
        implements DiscoverPresenter<V> {

    @Inject
    public DiscoverPresenterImpl(DataManager dataManager, CompositeDisposable compositeDisposable) {
        super(dataManager,compositeDisposable);
    }

    @Override
    public void fetchDiscoverMovies(String page) {

        getCompositeDisposable().add(getDataManager().getDiscoverMovies(BuildConfig.API_KEY, page)
                .timeInterval()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(new DisposableObserver<Timed<MovieModel>>() {
                    @Override
                    public void onComplete() {
                    }

                    @Override
                    public void onError(Throwable error) {
                        if (error instanceof java.net.UnknownHostException) {
                            getMvpView().onError("No internet connection",2);
                        }else{
                            getMvpView().onError("Service not available",2);
                        }
                    }

                    @Override
                    public void onNext(Timed<MovieModel> movieModelTimed) {
                        // getMvpView().hideLoading();
                        if(movieModelTimed.value()!=null)
                            getMvpView().onGettingDiscoverList(movieModelTimed.value());
                    }
                }));
    }
}
