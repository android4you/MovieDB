package com.android4you.movietmdb.ui.people.details.crew;

import android.net.Uri;
import android.view.View;
import android.widget.TextView;

import com.android4you.movietmdb.BuildConfig;
import com.android4you.movietmdb.R;
import com.android4you.movietmdb.data.model.people.MovieCreditsModel;
import com.android4you.movietmdb.ui.base.BaseViewHolder;
import com.facebook.drawee.view.SimpleDraweeView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by manu on 7/12/2018.
 */

public class PeopleCrewViewMovieHolder extends BaseViewHolder {

    View itemView;

    @BindView(R.id.my_image_view)
    SimpleDraweeView peopleProfileImage;

    @BindView(R.id.titleTV)
    TextView titleTV;

    MovieCreditsModel.CrewBean crewBean;

    public PeopleCrewViewMovieHolder(View itemView) {
        super(itemView);
        this.itemView = itemView;
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void bind(Object model) {
        crewBean = ( MovieCreditsModel.CrewBean)model;
        Uri uri = Uri.parse(BuildConfig.IMAGE_URL_SMALL +crewBean.getPoster_path());
        peopleProfileImage.setImageURI(uri);
        titleTV.setText(crewBean.getTitle());
    }
}
