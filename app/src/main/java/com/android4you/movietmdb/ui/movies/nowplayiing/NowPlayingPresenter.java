package com.android4you.movietmdb.ui.movies.nowplayiing;

import com.android4you.movietmdb.ui.base.MvpPresenter;

/**
 * Created by manu on 4/29/2018.
 */

public interface NowPlayingPresenter <V extends NowPlayingView> extends MvpPresenter<V> {
    void fetchNowPlayingMovies(String page);
}
