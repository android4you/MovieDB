package com.android4you.movietmdb.ui.movies.popular;

import com.android4you.movietmdb.ui.base.MvpPresenter;

/**
 * Created by Manu on 4/28/2018.
 */

public interface PopularPresenter<V extends PopularView> extends MvpPresenter<V> {
    void fetchPopularMovies(String page);
}
