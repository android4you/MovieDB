package com.android4you.movietmdb.ui.tvshows.toprated;

import android.net.Uri;
import android.view.View;
import android.widget.TextView;

import com.android4you.movietmdb.BuildConfig;
import com.android4you.movietmdb.R;
import com.android4you.movietmdb.data.model.TvShowResultModel;
import com.android4you.movietmdb.ui.base.BaseViewHolder;
import com.facebook.drawee.view.SimpleDraweeView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by manu on 4/29/2018.
 */

public class TopRatedTvViewHolder extends BaseViewHolder {

    TopRatedTvAdapter.OnResultClickListener onResultClickListener;

    View itemView;

    @BindView(R.id.my_image_view)
    SimpleDraweeView imageView;

    @BindView(R.id.titleTV)
    TextView titleTV;

    TvShowResultModel showResultModel;

    public TopRatedTvViewHolder(View itemView, TopRatedTvAdapter.OnResultClickListener onResultClickListener) {
        super(itemView);
        this.itemView = itemView;
        this.onResultClickListener = onResultClickListener;
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void bind(Object model) {
        showResultModel = (TvShowResultModel)model;
        Uri uri = Uri.parse(BuildConfig.IMAGE_URL_SMALL + showResultModel.getPoster_path());
        imageView.setImageURI(uri);
        titleTV.setText(showResultModel.getName());
    }

    @OnClick(R.id.movielistItemLayout)
    public void onMovieClick() {
        if (onResultClickListener != null) {
            onResultClickListener.onTVShowtClick(showResultModel, itemView);
        }
    }


}
