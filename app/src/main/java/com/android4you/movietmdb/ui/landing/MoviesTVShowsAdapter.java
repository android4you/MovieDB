package com.android4you.movietmdb.ui.landing;

import android.view.View;
import android.view.ViewGroup;

import com.android4you.movietmdb.R;
import com.android4you.movietmdb.data.model.ResultsModel;
import com.android4you.movietmdb.ui.base.BaseActivity;
import com.android4you.movietmdb.ui.base.BaseAdapter;
import com.android4you.movietmdb.ui.base.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Manu on 4/27/2018.
 */

public class MoviesTVShowsAdapter extends BaseAdapter {
    OnMovieClickListener itemClickListener;
    protected ArrayList<ResultsModel> productModelArrayList = new ArrayList<>();
    public MoviesTVShowsAdapter(BaseActivity activity) {
        super(activity);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MoviesTVShowViewHolder(getInflater().inflate(R.layout.movies_tvshow_row, parent, false),itemClickListener);
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.bind(productModelArrayList.get(position));
    }

    @Override
    public int getItemCount() {
        if (productModelArrayList == null || productModelArrayList.size() == 0) return 0;
        return productModelArrayList.size();
    }
    public void addItems(List<? extends ResultsModel> results) {
        if (results == null || results.size() == 0) return;
        int firstPosition = productModelArrayList.size() == 0 ? 0 : productModelArrayList.size() - 1;
        productModelArrayList.addAll(results);
        notifyItemRangeChanged(firstPosition, results.size());
    }

    public void setOnItemClickListener(OnMovieClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }
    interface OnMovieClickListener {
        void onMovieClick(ResultsModel productModel, View v);
    }

}