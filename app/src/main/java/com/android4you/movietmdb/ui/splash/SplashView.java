package com.android4you.movietmdb.ui.splash;


import com.android4you.movietmdb.ui.base.MvpView;

/**
 * Created by manu on 2/18/2018.
 */

public interface SplashView extends MvpView {
    void openMainActivity();
}

