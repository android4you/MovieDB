package com.android4you.movietmdb.ui.genres;

import com.android4you.movietmdb.data.DataManager;
import com.android4you.movietmdb.ui.base.BasePresenter;
import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by manu on 6/28/2018.
 */

public class GenresPresenterImpl<V extends GenresView> extends BasePresenter<V>
        implements GenresPresenter<V> {

    @Inject
    public GenresPresenterImpl(DataManager dataManager, CompositeDisposable compositeDisposable) {
        super(dataManager, compositeDisposable);
    }

    @Override
    public void fetchGenresrMovies(String page) {

    }
}