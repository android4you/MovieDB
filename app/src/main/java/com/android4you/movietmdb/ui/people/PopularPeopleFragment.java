package com.android4you.movietmdb.ui.people;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android4you.movietmdb.R;
import com.android4you.movietmdb.data.model.PopularPeopleModel;
import com.android4you.movietmdb.di.component.ActivityComponent;
import com.android4you.movietmdb.ui.base.BaseFragment;
import com.android4you.movietmdb.ui.home.HomeActivity;
import com.android4you.movietmdb.ui.people.details.PeopleDetailsActivity;
import com.android4you.movietmdb.widgets.MaterialProgressBar;
import com.google.gson.Gson;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by manu on 5/23/2018.
 */

public class PopularPeopleFragment extends BaseFragment implements PopularPeopleView, PopularPeopleAdapter.OnProductClickListener {
    @Inject
    PopularPeoplePresenter<PopularPeopleView> mPresenter;

    private HomeActivity homeActivity;
    @Inject
    LinearLayoutManager mLayoutManager;


    @BindView(R.id.popularRecyclerView)
    RecyclerView popularRecyclerView;

    @BindView(R.id.progress)
    MaterialProgressBar progress;

    @BindView(R.id.noInternetIV)
    ImageView noInternetIV;

    @BindView(R.id.swipyrefreshlayout)
    SwipyRefreshLayout mSwipyRefreshLayout;

    int page = 1;

    @Inject
    PopularPeopleAdapter popularAdapter;

    private View rootView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (rootView != null)
            return rootView;
        rootView = inflater.inflate(R.layout.fragment_popular, container, false);
        ActivityComponent component = getActivityComponent();

        if (component != null) {
            component.inject(this);
            mPresenter.onAttach(this);
        }
        homeActivity = (HomeActivity) getActivity();


        page = 1;
        ButterKnife.bind(this, rootView);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        popularRecyclerView.setLayoutManager(mLayoutManager);
        popularRecyclerView.setVisibility(View.VISIBLE);
        popularRecyclerView.setItemAnimator(new DefaultItemAnimator());
        popularRecyclerView.setAdapter(popularAdapter);
        popularAdapter.setOnItemClickListener(this);
        mPresenter.fetchPopulaPeoples(page+"");
        mSwipyRefreshLayout.setOnRefreshListener(direction -> {
            page++;
            mPresenter.fetchPopulaPeoples(page+"");
        });
        return rootView;
    }

    @Override
    protected void setUp(View view) {

    }

    @Override
    public void onResume() {
        super.onResume();
        homeActivity.headerTitle(getString(R.string.popularPeople));
    }

    @Override
    public void onGettingPopularPeople(PopularPeopleModel peopleModel) {
        if(peopleModel.getResults()!=null){
            progress.setVisibility(View.GONE);
            mSwipyRefreshLayout.setVisibility(View.VISIBLE);
            popularAdapter.addItems(peopleModel.getResults());

        }
    }

    @Override
    public void onError(String message, int type) {
        progress.setVisibility(View.GONE);
        noInternetIV.setVisibility(View.VISIBLE);
    }
    @Override
    public void onProductClick(PopularPeopleModel.ResultsBean resultsBean, View v) {
        Intent intent = new Intent(getActivity(), PeopleDetailsActivity.class);
        Gson gson = new Gson();
        String myJson = gson.toJson(resultsBean);
        intent.putExtra("myjson", myJson);
        startActivity(intent);
    }
}
