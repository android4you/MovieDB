package com.android4you.movietmdb.ui.landing;

import android.net.Uri;
import android.view.View;
import android.widget.TextView;

import com.android4you.movietmdb.BuildConfig;
import com.android4you.movietmdb.R;
import com.android4you.movietmdb.data.model.PopularPeopleModel;
import com.android4you.movietmdb.data.model.ResultsModel;
import com.android4you.movietmdb.ui.base.BaseViewHolder;
import com.facebook.drawee.generic.GenericDraweeHierarchyBuilder;
import com.facebook.drawee.generic.RoundingParams;
import com.facebook.drawee.view.SimpleDraweeView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by manu on 5/22/2018.
 */

public class PeopleViewHolder extends BaseViewHolder {
    PoepleAdapter.OnProductClickListener onProductClickListener;

    View itemView;

    @BindView(R.id.peopleProfileImage)
    SimpleDraweeView peopleProfileImage;

    @BindView(R.id.peopleName)
    TextView peopleName;

    PopularPeopleModel.ResultsBean productModel;


    public PeopleViewHolder(View itemView, PoepleAdapter.OnProductClickListener onProductClickListener) {
        super(itemView);
        this.itemView = itemView;
        this.onProductClickListener = onProductClickListener;
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void bind(Object model) {
        productModel = (PopularPeopleModel.ResultsBean)model;
        Uri uri = Uri.parse(BuildConfig.IMAGE_URL_SMALL +productModel.getProfile_path());
        peopleProfileImage.setImageURI(uri);
        int color = itemView.getResources().getColor(R.color.black);
        RoundingParams roundingParams = RoundingParams.fromCornersRadius(5f);
        roundingParams.setBorder(color, 1.0f);
        roundingParams.setRoundAsCircle(true);
        peopleProfileImage.getHierarchy().setRoundingParams(roundingParams);
        peopleName.setText(productModel.getName());
    }

    // @OnClick(R.id.card_view)
    public void onMovieClick() {
        if (onProductClickListener != null) {
            onProductClickListener.onProductClick(productModel, itemView);
        }
    }
}
