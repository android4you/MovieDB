package com.android4you.movietmdb.ui.landing;

import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android4you.movietmdb.BuildConfig;
import com.android4you.movietmdb.R;
import com.android4you.movietmdb.data.model.ResultsModel;
import com.android4you.movietmdb.ui.base.BaseViewHolder;
import com.facebook.drawee.view.SimpleDraweeView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Manu on 4/27/2018.
 */

public class MoviesTVShowViewHolder extends BaseViewHolder {

    MoviesTVShowsAdapter.OnMovieClickListener itemClickListener;

    View itemView;

   @BindView(R.id.my_image_view)
    SimpleDraweeView imageView;

    @BindView(R.id.titleTV)
    TextView titleTV;

    ResultsModel resultsModel;


    public MoviesTVShowViewHolder(View itemView, MoviesTVShowsAdapter.OnMovieClickListener itemClickListener) {
        super(itemView);
        this.itemView = itemView;
        this.itemClickListener = itemClickListener;
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void bind(Object model) {
        resultsModel = (ResultsModel)model;
        Uri uri = Uri.parse(BuildConfig.IMAGE_URL_SMALL +resultsModel.getPoster_path());
        imageView.setImageURI(uri);
        titleTV.setText(resultsModel.getTitle());

    }

    @OnClick(R.id.movielistItemLayout)
    public void onMovieClick() {
        Log.e("Clicking ....", ""+resultsModel.getTitle());
        if (itemClickListener != null) {
            itemClickListener.onMovieClick(resultsModel, itemView);
        }
    }

}