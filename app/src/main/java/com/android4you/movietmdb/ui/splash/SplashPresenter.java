package com.android4you.movietmdb.ui.splash;

import com.android4you.movietmdb.di.scope.PerActivity;
import com.android4you.movietmdb.ui.base.MvpPresenter;

/**
 * Created by manu on 2/18/2018.
 */


@PerActivity
public interface SplashPresenter<V extends SplashView> extends MvpPresenter<V> {
}

