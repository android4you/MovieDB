package com.android4you.movietmdb.ui.tvshows.popular;

import android.net.Uri;
import android.view.View;
import android.widget.TextView;

import com.android4you.movietmdb.BuildConfig;
import com.android4you.movietmdb.R;
import com.android4you.movietmdb.data.model.TvShowResultModel;
import com.android4you.movietmdb.ui.base.BaseViewHolder;
import com.facebook.drawee.view.SimpleDraweeView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by manu on 4/29/2018.
 */

public class PopularTvViewHolder extends BaseViewHolder {

    PopularTvAdapter.OnResultClickListener onResultClickListener;

    View itemView;

    @BindView(R.id.my_image_view)
    SimpleDraweeView imageView;

    @BindView(R.id.titleTV)
    TextView titleTV;

    TvShowResultModel tvShowResultModel;

    public PopularTvViewHolder(View itemView, PopularTvAdapter.OnResultClickListener onResultClickListener) {
        super(itemView);
        this.itemView = itemView;
        this.onResultClickListener = onResultClickListener;
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void bind(Object model) {
        tvShowResultModel = (TvShowResultModel)model;
        Uri uri = Uri.parse(BuildConfig.IMAGE_URL_SMALL  + tvShowResultModel.getPoster_path());
        imageView.setImageURI(uri);
        titleTV.setText(tvShowResultModel.getName());
    }

    @OnClick(R.id.movielistItemLayout)
    public void onMovieClick() {
        if (onResultClickListener != null) {
            onResultClickListener.onTVShowtClick(tvShowResultModel, itemView);
        }
    }


}
