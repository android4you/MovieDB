package com.android4you.movietmdb.ui.tvshows.toprated;

import com.android4you.movietmdb.ui.base.MvpPresenter;

/**
 * Created by manu on 4/29/2018.
 */

public interface TopRatedTvPresenter <V extends TopRatedTvView> extends MvpPresenter<V> {
    void fetchTopRatedTvs(String page);
}
