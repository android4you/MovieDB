package com.android4you.movietmdb.ui.movies.toprated;

import android.util.Log;

import com.android4you.movietmdb.BuildConfig;
import com.android4you.movietmdb.data.DataManager;
import com.android4you.movietmdb.data.model.MovieModel;
import com.android4you.movietmdb.ui.base.BasePresenter;
import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.schedulers.Timed;

/**
 * Created by manu on 4/29/2018.
 */

public class TopRatedPresenterImpl<V extends TopRatedView> extends BasePresenter<V> implements TopRatedPresenter<V> {

    @Inject
    public TopRatedPresenterImpl(DataManager controller, CompositeDisposable compositeDisposable) {
        super(controller, compositeDisposable);
    }

    @Override
    public void fetchTopRatedMovies(String page) {
         // getMvpView().showLoading();

        getCompositeDisposable().add(getDataManager().getTopRatedMovies(BuildConfig.API_KEY, page)
                .timeInterval()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(new DisposableObserver<Timed<MovieModel>>() {
                    @Override
                    public void onComplete() {
                    }

                    @Override
                    public void onError(Throwable error) {
                        if (error instanceof java.net.UnknownHostException) {
                            getMvpView().onError("No internet connection",1);
                        }else{
                            getMvpView().onError("Service not available",1);
                        }
                    }

                    @Override
                    public void onNext(Timed<MovieModel> movieModelTimed) {
                          //getMvpView().hideLoading();
                        if(movieModelTimed.value()!=null)
                            getMvpView().onGettingTopRatedList(movieModelTimed.value());
                    }
                }));
    }
}